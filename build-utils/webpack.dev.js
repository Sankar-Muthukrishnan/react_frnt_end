const commonPaths = require('./common-paths');
const webpack = require('webpack');
const port = process.env.PORT || 3009;
const config = {
    mode: 'development',
    entry: {
        app: ['babel-polyfill', `${commonPaths.appEntry}/index.js`]
    },
    output: {
        filename: '[name].[hash].js',
        publicPath: '/'
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.css$/,
                include: [/src/],
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                // localIdentName: '[local]'
                                localIdentName: '_[sha512:hash:base64:8]'
                            },
                            localsConvention: 'camelCase',
                            sourceMap: true
                        }
                    }
                ],
            },
            {
                test: /\.css$/,
                exclude: [/src/],
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    }
                ],
            },
            {
                test: /\.(gif|svg|jpg|png)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images/'
                        }
                    }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "MODE": JSON.stringify("dev"),
            "API_URL": JSON.stringify("http://localhost:3600"),
            "PREFIX_URL": JSON.stringify(""),
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
        host: 'localhost',
        port: port,
        historyApiFallback: true,
        hot: true,
        open: true
    }
};
module.exports = config;