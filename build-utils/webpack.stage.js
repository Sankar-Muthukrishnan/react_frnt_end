const commonPaths = require('./common-paths');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const config = {
    mode: 'stage',
    entry: {
        app: [`${commonPaths.appEntry}/index.js`]
    },
    output: {
        publicPath: "/admin/",
        filename: 'js/[name].[hash].js',
    },
    optimization: {
        minimizer: [
            // new UglifyJsPlugin({
            //     cache: true,
            //     parallel: true,
            //     sourceMap: true
            // }),
            new OptimizeCssAssetsPlugin({})
        ],
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                exclude: [/node_modules/],
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: '_[sha512:hash:base64:8]'
                            },
                            localsConvention: 'camelCase',
                            sourceMap: true
                        }
                    }]
                }),
            },
            {
                test: /\.css$/,
                exclude: [/src/],
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: 'css-loader'
                    }]
                }),
            },
            {
                test: /\.(gif|jpg|png)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images/'
                        }
                    }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'styles/styles.[hash].css',
            allChunks: true
        }),
        new webpack.DefinePlugin({
            "MODE": JSON.stringify("dev"),
            "API_URL": JSON.stringify("http://local.kgis.com/admin"),
            "PREFIX_URL": JSON.stringify("")
        })
    ]
};
module.exports = config;