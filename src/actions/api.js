import axios from 'axios';
import { get } from "lodash";
import Store from "../components/Store";
import { encrypt, decrypt, getAccessToken } from "../components/Utils";

var api = axios.create({
    baseURL: API_URL
});

api.interceptors.request.use(async (config) => {
    let keys = Store.getState().credentials.keys;
    let conf = {
        ...config,
        headers: {
            ...config.headers,
            'Authorization': 'Bearer ' + getAccessToken('access_token'),
            'X-Api-Key': keys.public
        }
    }

    let data = {
        'userId': getAccessToken('user_id')
    }

    if (config.data) {
        data = {
            ...data,
            ...config.data,
        }
    }
    conf["data"] = await encrypt(data, keys);
    return conf;
})

api.interceptors.response.use(async (response) => {
    let data = await decrypt(get(response, "data"), Store.getState().credentials.keys)
    return {
        ...response,
        data
    }
}, (error) => {
    return Promise.reject(error)
})

export default api;