import api from "./api";
import { getAccessToken } from "../components/Utils";
export function saveKeyPair(data) {
    return dispatch => {
        dispatch({
            type: "SAVE_KEYS",
            payload: data
        })
    }
}

export function submitLogin(data) {
    return dispatch => {
        dispatch({
            type: "LOGIN_FETCH",
            payload: api.post('/login', data)
        })
    }
}

export function getUser() {
    return dispatch => {
        dispatch({
            type: "USER",
            payload: api.get(`/users/${getAccessToken('user_id')}`)
        })
    }
}

export function getItems(attribute) {
    return dispatch => {
        dispatch({
            type: "FETCH_LIST",
            payload: api.get('/' + attribute),
            meta: { attribute }
        })
    }
}

export function getItem(attribute, id) {
    return dispatch => {
        dispatch({
            type: "LOAD_ITEM",
            payload: api.get('/' + attribute + "/" + { id }),
            meta: { attribute }
        })
    }
}

export function setupItem(attribute, data) {
    return dispatch => {
        dispatch({
            type: "SETUP_ITEM",
            payload: data,
            meta: { attribute }
        })
    }
}

export function addList(attribute, action, data) {

    return dispatch => {
        dispatch({
            type: "CRUD_FULFILLED",
            payload: { data: { status: 200, data: data } },
            meta: { attribute, crud_action: action }
        })
    }
}

export function submitItem(attribute, data) {
    return dispatch => {
        dispatch({
            type: "CRUD",
            payload: data.id ? api.post("/" + attribute + "/update", data) : api.post("/" + attribute + "/create", data),
            meta: { attribute, crud_action: data.id ? "update" : "create" }
        })
    }
}

export function itemActions(attribute, action, data) {
    return dispatch => {
        dispatch({
            type: "CRUD",
            payload: api.post("/" + attribute + "/" + action, data),
            meta: { attribute, crud_action: action }
        })
    }
}

export function loadMore(attribute, count, limit, data) {
    if (!data) {
        data = {}
    }
    return dispatch => {
        dispatch({
            type: "APPEND",
            payload: api.post('/' + attribute, {
                from: count,
                limit,
                ...data
            }),
            meta: { attribute }
        })
    }
}

export function headerButtons(data) {
    return dispatch => {
        dispatch({
            type: "HEADER_BUTTON",
            payload: data
        })
    }
}

export function sortItems(attribute, end, data) {
    if (end === true) {
        return dispatch => {
            dispatch({
                type: "ORDER",
                payload: data,
                meta: { attribute }
            })
        }
    }
    return getItems(attribute, data, "search");
}

export function alert(type, message) {
    if (typeof type === "string") {
        return dispatch => {
            dispatch({
                type: "ALERT",
                payload: {
                    type,
                    message,
                    time: new Date()
                }
            })
        }
    }
    return dispatch => {
        dispatch({
            type: "ALERT",
            payload: type
        })
    }
}

export function searchItems(attribute, end, data) {
    if (end === true) {
        return dispatch => {
            dispatch({
                type: "SEARCH",
                payload: data,
                meta: { attribute }
            })
        }
    }
    return getItems(attribute, data, "search");
}

export function submitLayoutItem(attribute, data) {
    return dispatch => {
        dispatch({
            type: "SAVE_ITEM",
            payload: api.post("/" + attribute + "/update", data),
            meta: { attribute }
        })
    }
}

export function getLayoutItem(attribute) {
    return dispatch => {
        dispatch({
            type: "FETCH_ITEM",
            payload: api.post('/' + attribute + "/view"),
            meta: { attribute }
        })
    }
}