import React from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, Field, reset } from 'redux-form';
import { required, email } from 'redux-form-validators';
import { Input, Button } from '~/components/Field';
import { connect } from "react-redux";
import style from '~/assets/css/page.css';
import logo from '~/assets/images/kgisl-gss-logo.svg';
import { forgotPassword } from '~actions'

class ForgotPassword extends React.Component {

    componentDidUpdate(prevProps) {
        if (prevProps.status !== this.props.status && this.props.status === 200 && this.props.fetching === false) {
            setTimeout(() => {
                this.props.dispatch(reset('forgotPassword'));
            }, 500);
        }
    }

    render() {
        const { fetching, status, invalid, onSubmit, handleSubmit, serverError, formData } = this.props;
        return (
            <div className={`${style.middleWrapTable} ${style.loginPage}`}>
                <div className={`${style.middle}`}>
                    <div className={`${style.c}`}>
                        <Link className={style.logo} to={`${PREFIX_URL}` + '/'} ><img src={logo} alt="" className={style.imgLogo} /></Link>
                        <div className={`${style.loginForm}`}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <Field type="email" name="email" component={Input} validate={[required({message: 'This field is required'}), email()]} label="Email" />
                                <div className={`${style.loginBtn} ${style.alignRight}`}>
                                    <Button classes={`${style.btnPrimary} ${style.btnCons}`} disabled={invalid} fetching={fetching}  submit={true} label={"Submit"} color={"#02269E"}  />
                                </div>
                            </form>
                             {
                                (fetching === false && status !== 200) && (
                                    <div className={`${style.helpMsg}`}>{formData.message}</div>
                                )
                            }
                            {
                                (fetching === false && status == 200) && (
                                    <div className={`${style.helpMsg} ${style.success}`}>{formData.message}</div>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    };
};

const mapStateToProps = (state) => {
    return {
        ...state.credentials
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit:(data) => {
            dispatch(forgotPassword(data));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'forgotPassword',
    enableReinitialize: true
})(ForgotPassword));