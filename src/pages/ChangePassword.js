import React from 'react';
import { get } from "lodash";
import { reduxForm, Field, reset } from 'redux-form';
import { required, length, format, confirmation } from 'redux-form-validators';
import { Input, Button } from '~/components/Field';
import { connect } from "react-redux";
import style from '~/assets/css/page.css';
import { toggle } from '../components/Utils';
// import { changePassword, userFetch } from '~actions'
import { toast } from 'react-toastify';

class ChangePassword extends React.Component {

     componentDidMount() {
        this.props.userFetch(this.props.match.params.id);
    }

     componentDidUpdate(prevProps) {
        if(prevProps.status !== this.props.status && this.props.status == 200 && this.props.fetching == false) {
            if(this.props.data !== undefined && typeof this.props.data.message !== 'undefined') {
                toast.success(this.props.data.message);
            }
            setTimeout(() => {
                this.props.dispatch(reset('changePassword'));
            }, 500);
        }
    }

    toggle = () => {
        toggle(`${style.formWrap}`);
    }
    render() {
        const { fetching, status, invalid, onSubmit, handleSubmit, serverError, formData, match } = this.props;
         const onPassword = (values) => {
            onSubmit(values, match.params.id)
        };
        return (
            <div className={`${style.contentRight} ${style.userFormAr}`} >
                <div className={style.row}>
                    <div className={style.tableHead} onClick={() => this.toggle()}>
                        <h4 className={style.tableHeadTitle}>Change Password for {get(formData, "fullname", '--')}</h4>
                    </div>
                    <div className={`${style.formWrap}`}>
                        <form onSubmit={handleSubmit(onPassword)}>
                            <div className={style.formFields}>
                                <Field type="text" name="password" helpmsg="yes" label="Password" component={Input} validate={[required({message: 'This field is required'}), length({ minimum: 8, message: 'Mininum 8 character'}), format({ with: /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[#?!@$%^&*-])/, message: 'Should contains: [A-Z] [a-z] [0-9] [#?!@$%^&*-]' })]} />
                                 <Field type="text" name="password_repeat" helpmsg="yes" label="Password Repeat" component={Input} validate={[required({message: 'This field is required'}), length({ minimum: 8, message: 'Mininum 8 character' }), format({ with: /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[#?!@$%^&*-])/, message: 'Should contains: [A-Z] [a-z] [0-9] [#?!@$%^&*-]' }), confirmation({ field: 'password', message: 'Password does not match' })]} />
                            </div>
                            <div className={`${style.formActions}`}>
                                <Button classes={`${style.btnPrimary} ${style.btnCons}`} disabled={invalid} fetching={fetching} submit={true} label={"Save"} />
                                <Button classes={`${style.btnPlain} ${style.btnCons}`} link={`${PREFIX_URL}` + `/user`}label={"Cancel"} />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    };
}


const mapStateToProps = (state) => {
    return {
        ...state.list.user,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit:(data, id) => {
            dispatch(changePassword(data, id));
        },
         userFetch: (id) => {
            dispatch(userFetch(id));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'changePassword',
    enableReinitialize: true
})(ChangePassword));