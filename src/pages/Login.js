import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { required } from 'redux-form-validators';
import { Input, Button } from '~/components/Field';
import { connect } from "react-redux";
import style from '~/assets/css/page.css';
import logo from '~/assets/images/kgisl-gss-logo.svg';
import { submitLogin } from '~/actions';
import { setAccessToken, isLoggedIn } from '~/components/Utils';

class Login extends React.Component {
    componentDidMount() {
        if (isLoggedIn()) {
            this.props.history.push(`${PREFIX_URL}/home`);
        }

    }

    componentDidUpdate(prevProps) {
        if (prevProps.status !== this.props.status && this.props.status == 200 && this.props.fetching == false) {
            let token = this.props.formData.accessToken;
            let userId = this.props.formData.userId;
            setAccessToken(token, 'access_token');
            setAccessToken(userId, 'user_id')
            window.location = `${PREFIX_URL}/admin`;
        }
    }

    render() {
        const { fetching, status, invalid, onSubmit, handleSubmit, serverError } = this.props;
        return (
            <div className={`${style.middleWrapTable} ${style.loginPage}`}>
                <div className={`${style.middle}`}>
                    <div className={`${style.c}`}>
                        <Link className={style.logo} to={`${PREFIX_URL}` + '/'} ><img src={logo} alt="" className={style.imgLogo} /></Link>
                        <div className={`${style.loginForm}`}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <Field type="text" name="username" component={Input} helpmsg="yes" validate={[required({ message: 'This field is required' })]} label="Username" serverError={serverError["loginform-username"]} />
                                <Field type="password" name="password" component={Input} helpmsg="yes" validate={[required({ message: 'This field is required' })]} label="Password" serverError={serverError["loginform-password"]} />
                                <div className={`${style.formGroup} ${style.trouble}`}>
                                    <Link to={'/forgot-password'}>Trouble login in?</Link>
                                    <div className={`${style.reminder}`}>
                                        <input type="checkbox" id="reminder" value={1} />
                                        <label htmlFor="reminder" className={style.checkboxSpan}>Keep me loggedin</label>
                                    </div>
                                </div>
                                <div className={`${style.loginBtn} ${style.alignRight}`}>
                                    <Button classes={`${style.btnPrimary} ${style.btnCons}`} disabled={invalid} fetching={fetching} submit={true} label={"Login"} color={"#02269E"} />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
};

const mapStateToProps = (state) => {
    return {
        ...state.credentials
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (data) => {
            dispatch(submitLogin(data));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'login',
    enableReinitialize: true
})(withRouter(Login)));