import React from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, Field, reset } from 'redux-form';
import { required, length, format, confirmation } from 'redux-form-validators';
import { Input, Button } from '~/components/Field';
import { connect } from "react-redux";
import style from '~/assets/css/page.css';
import logo from '~/assets/images/kgisl-gss-logo.svg';
import { resetPassword } from '~actions'

class ResetPassword extends React.Component {

    componentDidMount() {
        this.props.dispatch(reset('resetPassword'));
    }

    componentDidUpdate(prevProps) {
         if (prevProps.status !== this.props.status && this.props.status === 200 && this.props.fetching === false) {
            setTimeout(() => {
                window.location = `${PREFIX_URL}/`;
            }, 2500);
        }
    }

    render() {
        const { fetching, status, invalid, onSubmit, handleSubmit, serverError, formData, formSubmit, match } = this.props;
        const os = (values) => {
            formSubmit(values, match.params.id)
        };
        return (
            <div className={`${style.middleWrapTable} ${style.loginPage}`}>
                <div className={`${style.middle}`}>
                    <div className={`${style.c}`}>
                        <Link className={style.logo} to={`${PREFIX_URL}` + '/'} ><img src={logo} alt="" className={style.imgLogo} /></Link>
                        <div className={`${style.loginForm}`}>
                            <form onSubmit={handleSubmit(os)}>
                                <Field type="text" name="password" helpmsg="yes" label="Password" component={Input} validate={[required({message: 'This field is required'}), length({ minimum: 8, message: 'Mininum 8 character'}), format({ with: /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[#?!@$%^&*-])/, message: 'Should contains: [A-Z] [a-z] [0-9] [#?!@$%^&*-]' })]} />
                                 <Field type="text" name="password_repeat" helpmsg="yes" label="Password Repeat" component={Input} validate={[required({message: 'This field is required'}), length({ minimum: 8, message: 'Mininum 8 character' }), format({ with: /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[#?!@$%^&*-])/, message: 'Should contains: [A-Z] [a-z] [0-9] [#?!@$%^&*-]' }), confirmation({ field: 'password', message: 'Password does not match' })]} />
                                <div className={`${style.loginBtn} ${style.alignRight}`}>
                                    <Button classes={`${style.btnPrimary} ${style.btnCons}`} disabled={invalid} fetching={fetching}  submit={true} label={"Submit"} color={"#02269E"}  />
                                </div>
                            </form>
                            {
                                (fetching === false && status !== 200) && (
                                    <div className={`${style.helpMsg}`}>{formData.message}</div>
                                )
                            }
                            {
                                (fetching === false && status == 200) && (
                                    <div className={`${style.helpMsg} ${style.success}`}>{formData.message}</div>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    };
};

const mapStateToProps = (state) => {
    return {
        ...state.credentials
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        formSubmit: (data, id) => {
            let obj = {
                "id": id,
                "password": data.password,
                "password_repeat": data.password_repeat
            }
            dispatch(resetPassword(obj));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'resetPassword',
    enableReinitialize: true
})(ResetPassword));