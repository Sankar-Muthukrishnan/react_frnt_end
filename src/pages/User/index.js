import React from 'react';
import Grid from '../../components/Grid';
import style from '~/assets/css/page.css';
import { Link } from 'react-router-dom';

export default class User extends React.Component {
    render() {
        return (
            <div className={`${style.contentRight} ${style.userList}`}>
                <div className={style.row}>
                    <Grid
                        attribute="users"
                        title="Users"
                        columns={{
                            "fullname": "Name",
                            "email": "Email"
                        }}
                        buttons={(
                            <Link to={`/users/create`} className={style.optionLink}>
                                <i className={`fal fa-plus ${style.icn}`}></i>
                                <span>Add</span>
                            </Link>
                        )}
                    />
                </div>
            </div>
        )
    }
}