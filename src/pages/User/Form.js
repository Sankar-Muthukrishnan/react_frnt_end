import React from 'react';
import { get } from 'lodash';
import { connect } from "react-redux";
import { Field } from 'redux-form';
import { required, length, format, confirmation, email } from 'redux-form-validators';
import GridForm from '../../components/Grid/Form';
import { Input } from '~/components/Field';
import style from '~/assets/css/page.css';
import Upload from '~/components/Upload';
import SingleFile from '~/components/SingleFile';
import { headerButtons } from '../../actions';


class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: get(props, "match.params.id", false)
        }
    }

    componentDidMount() {
        this.props.headerButtons(
            <>

            </>
        );
    }

    render() {
        const { serverError = [], match } = this.props;
        return (
            <div className={`${style.contentRight} ${style.userList}`}>
                <div className={style.row}>
                    <GridForm title="User" attribute="users" match={match} history={this.props.history}>
                        <Field type="text" name="firstName" component={Input} helpmsg="yes" label="Firstname" serverError={serverError["admin-first_name"]} />
                        <Field type="text" name="lastName" component={Input} helpmsg="yes" validate={[required({ message: 'This field is required' })]} label="Surname" serverError={serverError["admin-surname"]} />
                        <Field type="text" name="email" component={Input} helpmsg="yes" validate={[required({ message: 'This field is required' }), email({ message: 'Please fill valid email address' })]} label="Email" serverError={serverError["admin-email"]} />
                        {
                            this.state.id === false && (
                                <>
                                    <Field type="text" name="password" component={Input} helpmsg="yes" validate={[required({ message: 'This field is required' }), length({ minimum: 8, message: 'Mininum 8 character' }), format({ with: /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[#?!@$%^&*-])/, message: 'Should contains: [A-Z] [a-z] [0-9] [#?!@$%^&*-]' })]} label="Password" serverError={serverError["admin-password"]} />
                                </>
                            )
                        }
                        <div className={`${style.formGroup}`}>
                            <div className={`${style.userUploadHelp}`}>The image dimension should be a minimum of <span>140*140</span></div>
                            <div className={style.pRea}>
                                <a className={`${style.fileUpload}`}>
                                    <label className={`${style.button} ${style.btnPrimary}`} htmlFor="image_id"><i className="fa fa-upload"></i><span>Upload File</span></label>
                                </a>
                                <Upload clear={true} id="image_id" multiple={false} renderItem={SingleFile} onComplete={(response) => { this.props.change('image_id', response.data.id) }} />
                            </div>
                            <div className={style.picture}>
                                <span className={`${style.userImage} fa fa-user`}></span>
                            </div>
                        </div>
                    </GridForm>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        headerButtons: (data) => { dispatch(headerButtons(data)) }
    }
}

export default connect(null, mapDispatchToProps)(Form);