import React from 'react';
import style from '~/assets/css/page.css';
import { connect } from "react-redux";
import { saveUser } from '~/actions';
import Tile from '../components/Tile';
import Recentposts from '../components/Recentposts';
import RecentPage from '../components/RecentPage';
import { DragDropContext } from 'react-beautiful-dnd';
import { get } from 'lodash';
import Analytics from '../components/Analytics';
import Map from '../components/Map';
import { makeHeightByRow } from '../components/Utils';
import { headerButtons } from '../actions';
import Search from '../components/Grid/Search';
import { Link } from 'react-router-dom';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tile: this.tiles(3),
            recentPost: this.recentpost(3),
            recentPage: this.recentpage(3),
        };
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    componentDidMount() {
        const { headerButtons, buttons = false, end } = this.props;
        makeHeightByRow(`.${style.withMap}>.${style.col}`);
        headerButtons(
            <>
                <Search attribute={'home'} end={end} />
            </>
        )

    }
    // Fake data of Tiles    
    tiles = (count) => Array.from({ length: count }, (v, k) => k).map(k => ({
        id: k,
        title: `title ${k}`,
        overall: "2415",
        today: "751",
        monthly: "1547",
        percentage: "4%"
    }));


    // Fake data of Recent posts
    recentpost = (count) => Array.from({ length: count }, (v, k) => k).map(k => ({
        id: k,
        title: `Recent Post ${k}`,
        content: 'The attention to detail and the end product is stellar! I enjoyed the process',
        pin: 'test'
    }));

    // Fake data of Recent Pages
    recentpage = (count) => Array.from({ length: count }, (v, k) => k).map(k => ({
        id: k,
        title: `page ${k}`,
        pin: null
    }))

    // Sorting of droppable
    reorder = (list, startIndex, endIndex) => {
        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);
        return result;
    }

    // Drag end function
    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }
        if (result.type === "recentPost") {
            const recentPost = this.reorder(
                this.state.recentPost,
                result.source.index,
                result.destination.index
            );
            this.setState({
                recentPost,
            });
        }
        else if (result.type === "recentPage") {
            const recentPage = this.reorder(
                this.state.recentPage,
                result.source.index,
                result.destination.index
            );
            this.setState({
                recentPage,
            });
        }
        else {
            const tile = this.reorder(
                this.state.tile,
                result.source.index,
                result.destination.index
            );
            this.setState({
                tile,
            });
        }
    }

    pinItem = (id) => {
        $(`#` + id).toggleClass(`${style.pinnedItem}`);
    }

    render() {
        const { } = this.props;
        return (
            <div className={`${style.contentRight}`}>
                <div className={style.row}>
                    <DragDropContext onDragEnd={this.onDragEnd}>
                        <div className={style.divider}>
                            <Tile tiles={this.state.tile} />
                        </div>
                        <div className={`${style.divider}`}>
                            <Recentposts profile={JSON.parse(window.localStorage.getItem('profile'))} recentposts={this.state.recentPost} pinnable={(id) => this.pinItem(id)} />
                        </div>
                        <div className={`${style.divider}`}>
                            <RecentPage recentpages={this.state.recentPage} pin={this.pin} pinnable={(id) => this.pinItem(id)} />
                        </div>
                        <div className={`${style.divider} ${style.withMap}`}>
                            <div className={`${style.col}`}>
                                <Analytics />
                            </div>
                            <div className={`${style.mapAr} ${style.col}`}>
                                <Map />
                            </div>
                        </div>
                    </DragDropContext>
                </div>
            </div>
        )
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        headerButtons: (data) => { dispatch(headerButtons(data)) }
    }
}

export default connect(null, mapDispatchToProps)(Home);