import React from 'react';
import style from '~/assets/css/page.css';

class NoMatch extends React.Component {
    render() {
        return (
            <div className={`${style.contentRight}`}>
                <div className={`${style.errorPage} ${style.middleWrapTable}`}>
                    <div className={style.middle}>
                        <h1 className={style.errorNumber}>404</h1>
                        <div className={style.errorMessage}>Oops! That page can't be found.</div>
                        <div className={style.errorMessage}>It looks like nothing was found at this location.</div>
                    </div>
                </div>
            </div>
        );
    }
};

export default NoMatch;