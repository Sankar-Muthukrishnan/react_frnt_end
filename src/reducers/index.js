import { combineReducers } from "redux";
import { reducer as forms } from 'redux-form';
import credentialsReducer from './credentialsReducer';
import listReducer from './listReducer';
import layoutReducer from './layoutReducer';

export default combineReducers({
    form: forms,
    credentials: credentialsReducer,
    list: listReducer,
    layout: layoutReducer
})