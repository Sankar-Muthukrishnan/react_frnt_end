import { get } from "lodash";

let defaultState = {
	fetching: false,
	status: false,
	data: {},
	action: "",
	serverError: []
}

export default function layoutReducer(state = {
	header: defaultState,
	footer: defaultState,
	popup: defaultState,
}, action) {
	switch (action.type) {
		case "FETCH_ITEM_PENDING": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...defaultState,
					action: "fetch",
					data: {},
					fetching: true
				}
			}
		}
		case "FETCH_ITEM_REJECTED":
		case "SAVE_ITEM_REJECTED": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...defaultState
				}
			}
		}
		case "FETCH_ITEM_FULFILLED": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...defaultState,
					action: "fetch",
					status: get(action.payload.data, "status", 0),
					data: get(action.payload.data, "data", {}),
				}
			}
		}
		case "SAVE_ITEM_PENDING": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...state[attribute],
					status: false,
					fetching: true
				}
			}
		}
		case "SAVE_ITEM_FULFILLED": {
			const { attribute } = action.meta;
			let status = get(action.payload.data, "status", 400);
			let data = get(action.payload.data, "data", {});
			if (status === 200) {
				return {
					...state,
					[attribute]: {
						status,
						data,
						action: "update",
						fetching: false
					}
				}
			}
			return {
				...state,
				[attribute]: {
					...state[attribute],
					action: "update",
					status: status,
					fetching: false,
					serverError: data
				}
			}
		}
	}
	return state;
}