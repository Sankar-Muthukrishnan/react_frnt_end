import { get } from "lodash";

export default function credentialsReducer(state = {
    fetching: false,
    user: false,
    status: null,
    formData: {},
    serverError: [],
    keys: false,
}, action) {
    switch (action.type) {
        case "SAVE_KEYS": {
			return {
				...state,
				keys: action.payload
			}
		}
        case "LOGIN_FETCH_PENDING":
        case "FORGOT_PASSWORD_PENDING":
        case "RESET_PASSWORD_PENDING": {
            return { ...state, fetching: true }
        }
        case "LOGIN_FETCH_REJECTED":
        case "FORGOT_PASSWORD_REJECTED":
        case "RESET_PASSWORD_REJECTED": {
            return { ...state, fetching: false }
        }
        case "LOGIN_FETCH_FULFILLED":
        case "FORGOT_PASSWORD_FULFILLED":
        case "RESET_PASSWORD_FULFILLED": {
            if (get(action.payload.data, "status") != 200) {
                return {
                    ...state,
                    fetching: false,
                    serverError: get(action.payload.data, "data"),
                    formData: get(action.payload.data, "data"),
                    status: get(action.payload.data, "status"),
                }
            }
            return {
                ...state,
                fetching: false,
                formData: get(action.payload.data, "data"),
                status: get(action.payload.data, "status"),
                serverError: []
            }
        }
        case "USER_PENDING":
        case "USER_REJECTED": {
            return {
                ...state
            }
        }
        case "USER_FULFILLED": {
            let d = action.payload.data;
            if (get(d, "status") === 200) {
                return {
                    ...state,
                    user: get(d, "data.id", false) ? get(d, "data") : false
                }
            }
        }
    }
    return state;
}