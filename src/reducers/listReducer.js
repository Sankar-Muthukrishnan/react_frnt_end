import { get, orderBy } from "lodash";

let defaultState = {
	fetching: false,
	status: false,
	data: [],
	order: {
		field: "id",
		sort_order: "desc"
	},
	original: [],
	list: [],
	item: {},
	serverError: [],
	end: true,
	total: 0,
	action: ''
}
export default function listReducer(state = {
	users: defaultState,
}, action) {
	switch (action.type) {
		case "FETCH_LIST_PENDING": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...defaultState,
					item: {},
					action: action.meta.action,
					fetching: true
				}
			}
		}
		case "FETCH_LIST_REJECTED":
		case "CRUD_REJECTED":
		case "APPEND_REJECTED": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...defaultState
				}
			}
		}
		case "FETCH_LIST_FULFILLED": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...defaultState,
					status: get(action.payload.data, "status", 0),
					data: get(action.payload.data, "data", []),
					original: get(action.payload.data, "data.results", []),
					list: get(action.payload.data, "data.results", []),
					total: get(action.payload.data, "data.mastertotal"),
					end: get(action.payload.data, "data.mastertotal") < 20 ? true : false,
				}
			}
		}
		case "APPEND_PENDING": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...state[attribute],
					action: action.meta.action,
					fetching: true,
					status: false
				}
			}
		}
		case "APPEND_FULFILLED": {
			const { attribute } = action.meta;
			let d = get(action.payload.data, "data.results", []);
			return {
				...state,
				[attribute]: {
					...state[attribute],
					fetching: false,
					list: [
						...state[attribute].list,
						...d
					],
					end: d.length < 20 ? true : false,
					status: get(action.payload.data, "status", 400)
				}
			}
		}
		case "SEARCH": {
			const { attribute } = action.meta;
			let data = state[attribute]["original"];
			let term = get(action.payload, "search", "").trim().toLowerCase();
			let result = [];
			if (term === "") {
				result = data;
			}
			else if (term.trim() !== "" && data.length > 0) {
				let d = data[0];
				result = data.filter(item => {
					let matches = 0;
					Object.keys(d).map(key => {
						if (["id", "deleted", "created_at", "updated_at"].includes(key)) {
							//Do nothing
						}
						else {
							let field = typeof item[key] == "string" ? item[key] : JSON.stringify(item[key]);
							if (field.trim().toLowerCase().indexOf(term) !== -1) {
								matches += 1;
								return false;
							}
						}
					})
					return matches !== 0 ? true : false;
				})
			}
			return {
				...state,
				[attribute]: {
					...state[attribute],
					action: "search",
					list: result
				}
			}
		}
		case "CRUD_PENDING": {
			const { attribute, crud_action } = action.meta;
			return {
				...state,
				[attribute]: {
					...state[attribute],
					action: crud_action,
					status: false,
					fetching: true
				}
			}
		}
		case "CRUD_FULFILLED": {
			const { attribute, crud_action } = action.meta;
			let status = get(action.payload.data, "status", 400);
			if (status === 200) {
				let new_data = state[attribute].list;
				let new_original_data = state[attribute].original;
				if (crud_action === "create") {
					if (get(action.payload.data, "data", false) !== false) {
						new_data = [
							action.payload.data.data,
							...new_data
						];
					}
				}
				else if (attribute !== 'image' && (crud_action === "delete" || crud_action === "restore")) {
					let data = get(action.payload.data, "data", []);
					data.map(item => {
						let index = new_data.findIndex(d => item.id === d.id);
						if (index >= 0) {
							new_data.splice(index, 1, item);
						}
					});
					if (attribute === 'page') {
						new_original_data = new_data;
					}
				}
				else if (attribute === 'image' && crud_action === "delete") {
					let data = get(action.payload.data, "data.updated", []);
					data.map(item => {
						let index = new_data.findIndex(d => item === d.id);
						if (index >= 0) {
							new_data.splice(index, 1);
						}
					});
				}
				else if (crud_action === "hide") {
					let data = get(action.payload.data, "data", []);
					data.map(item => {
						new_data = new_data.filter(d => item.id !== d.id)
					});
					if (attribute === 'page') {
						new_original_data = new_data;
					}
				}
				else if (crud_action === "visibility") {
					let data = get(action.payload.data, "data", {});
					new_data.find(d => data.image_id === d.id).visible = data.visible;
				}
				else if (crud_action === "update") {
					let data = get(action.payload.data, "data", {});
					let index = new_data.findIndex(d => data.id === d.id);
					if (index >= 0) {
						new_data.splice(index, 1, data);
					}
				}
				else if (crud_action === "sort") {
					let type = get(action.payload.data, "data.type", {});
					new_data = orderBy(new_data, ['name'], [type]);
				}
				return {
					...state,
					[attribute]: {
						...state[attribute],
						fetching: false,
						status: get(action.payload.data, "status", 400),
						data: get(action.payload.data, "data", 400),
						action: crud_action,
						list: [
							...new_data
						],
						original: [
							...new_original_data
						],
						serverError: []
					}
				}
			}
			return {
				...state,
				[attribute]: {
					...state[attribute],
					status: get(action.payload.data, "status", 400),
					fetching: false,
					serverError: get(action.payload.data, "data", [])
				}
			}
		}
		case "ORDER": {
			const { attribute } = action.meta;
			const { field, sort_order } = action.payload;
			return {
				...state,
				[attribute]: {
					...state[attribute],
					order: {
						...action.payload
					},
					list: orderBy(state[attribute].original, [field], [sort_order])
				}
			}
		}
		case "LOAD_ITEM_PENDING": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...state[attribute],
					fetching: true,
					item: {}
				}
			}
		}
		case "LOAD_ITEM_REJECTED": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...state[attribute],
					fetching: false
				}
			}
		}
		case "LOAD_ITEM_FULFILLED": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...state[attribute],
					fetching: false,
					item: get(action.payload.data, "data", {})
				}
			}
		}
		case "SETUP_ITEM": {
			const { attribute } = action.meta;
			return {
				...state,
				[attribute]: {
					...state[attribute],
					item: action.payload
				}
			}
		}
	}
	return state;
}