import React from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import { ToastContainer, toast, Slide } from 'react-toastify';
import { connect } from "react-redux";
import { SodiumPlus, X25519PublicKey } from "sodium-plus";

import { PrivateRoute } from '~/components/Auth/PrivateRoute';
import { saveKeyPair } from './actions';
import NoMatch from './pages/NoMatch';

import Login from './pages/Login';
import ForgotPassword from './pages/ForgotPassword';
import ResetPassword from './pages/ResetPassword';
import Dashboard from './pages/Dashboard';
import User from './pages/User';
import UserForm from './pages/User/Form';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-toastify/dist/ReactToastify.css';
import "./assets/js/matchHeight.js";
import style from '~/assets/css/page.css';


class App extends React.Component {
	async componentDidMount() {
		if (MODE !== "dev" && !window.sodium) {
			window.sodium = await SodiumPlus.auto();
			//Key pair for client
			let pair = await sodium.crypto_box_keypair();
			let pub = await sodium.crypto_box_publickey(pair);
			//Save it locally
			this.props.saveKeyPair({
				secret: await sodium.crypto_box_secretkey(pair),
				public: await sodium.sodium_bin2hex(pub.getBuffer()),
				shared_public: X25519PublicKey.from(PUBLIC_KEY, 'hex')
			})
		}
	}

	render() {
		const { keys } = this.props;
		if (keys === false && MODE !== "dev") {
			return null;
		}
		return (
			<Router basename="/admin" history={history}>
				<div className={style.viewport}>
					<Route render={({ location }) => (
						<Switch location={location}>
							<Route exact path={`${PREFIX_URL}/login`} component={Login} />
							{/* <Route exact path={`${PREFIX_URL}/forgot-password`} component={ForgotPassword} /> */}
							{/* <Route exact path={`${PREFIX_URL}/reset-password/:id`} component={ResetPassword} /> */}
							<PrivateRoute exact path={`${PREFIX_URL}/dashboard`} component={Dashboard} />
							<PrivateRoute exact path={`${PREFIX_URL}/users`} component={User} />
							<PrivateRoute exact path={`${PREFIX_URL}/users/create`} component={UserForm} />
							<PrivateRoute exact path={`${PREFIX_URL}/users/:id`} component={UserForm} />
							<PrivateRoute component={NoMatch} />
						</Switch>
					)}
					/>
				</div>
				<ToastContainer draggable={false} position={toast.POSITION.TOP_RIGHT} autoClose={6000} transition={Slide} />
			</Router>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		...state.credentials
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		saveKeyPair: (data) => {
			dispatch(saveKeyPair(data));
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
