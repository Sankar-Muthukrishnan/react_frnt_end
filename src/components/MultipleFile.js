import React from 'react';
import Spinner from './Spinner';
import { get } from 'lodash';
import style from '~/assets/css/page.css';
import video from '~/assets/images/video.png';
import audio from '~/assets/images/audio.png';
import Background from '~/components/Background';

export default class MultipleFile extends React.Component {
    render() {
        const { remove, valid, success, data, complete, progress, onRemoveHandler } = this.props;
        if (remove || !valid) {
            return null;
        }
        return (
             <div className={style.mediaRow}>
                <div className={style.media}>
                    <div className={style.pRea}>
                        <div className={style.mediaSizer}></div>
                        {
                            complete === false && (
                                <Spinner color="#000" />
                            )
                        }
                        {
                            success === true && data && (
                                <Background prefix={get(data,'prefix')} type={get(data,'type')} orgname={get(data,'orgname')} name={get(data,'name')} extension={get(data,'extension')} size="100x100" />
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}
      