import React from 'react';
import style from '~/assets/css/page.css';
import { connect } from 'react-redux';
import { Line } from 'react-chartjs-2';

class Analytics extends React.Component {
    render() {
        const charData = {
            labels: ['', '', '', '', '', '', '', '', '', '', '', ''],
            datasets: [
                {
                    label: 'old',
                    fill: false,
                    lineTension: 0.1,
                    borderColor: 'rgba(213, 215, 217, 1)',
                    pointBorderColor: 'rgba(0,0,0,0)',
                    pointBackgroundColor: 'transparent',
                    pointHoverBackgroundColor: 'transparent',
                    pointHoverBorderColor: 'transparent',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [37, 33, 50, 58, 52, 60, 72, 88, 72, 78, 80, 95],
                    spanGaps: true,
                }, {
                    label: 'new',
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(131, 207, 192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'transparent',
                    pointBackgroundColor: 'transparent',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'transparent',
                    pointHoverBorderColor: 'rgba(75,192,192,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [35, 30, 45, 55, 50, 65, 60, 75, 85, 70, 75, 91],
                    spanGaps: false,
                }
            ]
        };

        return (
            <div className={style.graphArWrap}>
                <div className={`${style.graphTextAr} ${style.commonWrapper}`}>
                    <p>Analytics</p>
                    <div className={style.row}>
                        <div className={style.col}>
                            <div className={style.medText}>Total Income</div>
                            <div>$15,354</div>
                        </div>
                        <div className={style.col}>
                            <div className={style.smallText}>Total due</div>
                            <div>$4,653</div>
                        </div>
                        <div className={style.col}>
                            <input type="text" placeholder="Search" />
                        </div>
                    </div>
                    <div className={style.secondRow}>
                        <div className={style.col}>
                            <div className={style.colorText}>Open</div>
                            <div>16:203.26</div>
                        </div>
                        <div className={style.col}>
                            <div className={style.colorText} >Day Range</div>
                            <div>01.12.13 - 01.01.14</div>
                        </div>
                        <div className={style.col}>
                            <div className={style.innerRow}>
                                <div>Cash</div>
                                <div className={`${style.colorText} ${style.right}`}>$10,525</div>
                            </div>
                            <div className={style.innerRow}>
                                <div>Visa Classic</div>
                                <div className={`${style.colorText} ${style.right}`}>$5,989</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`${style.relative} ${style.graphArea}`}>
                    <Line ref="chart" data={charData} options={{
                        tooltips: {
                            enabled: false
                        },
                        legend: {
                            display: false
                        },
                        responsive: true,
                        scales: {
                            yAxes: [
                                {
                                    ticks: {
                                        autoSkip: true,
                                        maxTicksLimit: 10,
                                        beginAtZero: true,
                                        display: false

                                    },
                                    gridLines: {
                                        display: false
                                    },
                                }
                            ],
                            xAxes: [
                                {
                                    gridLines: {
                                        display: false
                                    },
                                    label: {
                                        display: false
                                    }
                                }
                            ]
                        },
                    }} />
                </div>
            </div >
        );
    };
};

export default connect()(Analytics);