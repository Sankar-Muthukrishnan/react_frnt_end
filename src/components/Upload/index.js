import React from 'react';
import PropTypes from "prop-types";
import UploadFile from "./UploadFile";
import style from '~/assets/css/page.css';
import fa from '~/assets/css/font-awesome.css';

class Upload extends React.Component {
    constructor(props) {
        super(props);
        this._dragged = false;
        this.state = {
            uploads: [],
            clear: props.clear || false
        }
    }

    componentDidMount() {
        this.enableDrag();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.clear !== this.props.clear && this.props.clear === true) {
            this.setState({
                uploads: []
            })
        }
    }

    componentWillUnmount() {
        this.disableDrag();
        clearTimeout(this._dragged);
    }

    enableDrag = () => {
        let handlerFunc = this.onDropHandler;
        $(`.${style.addnew}`).bind({
            dragover: function (e) {
                e.stopPropagation();
                e.preventDefault();
                clearTimeout(this._dragged);
                let dt = e.originalEvent.dataTransfer;
                if (((dt.types && (dt.types.indexOf ? dt.types.indexOf('Files') !== -1 : dt.types.contains('Files'))) || (dt.mozSourceNode === null))) {
                    $(`.${style.addnew}`).addClass(`${style.incoming}`);
                }
            },
            drop: function (event) {
                event.stopPropagation();
                event.preventDefault();
                let files = event.originalEvent.dataTransfer.files;
                handlerFunc(files);
                $(`.${style.addnew}`).removeClass(`${style.incoming}`);
            },
            dragleave: function (e) {
                this._dragged = setTimeout(function () {
                    e.stopPropagation();
                    $(`.${style.addnew}`).removeClass(`${style.incoming}`);
                }, 100);
            }
        });
    }

    disableDrag = () => {
        $(`.${style.addnew}`).unbind("dragover");
        $(`.${style.addnew}`).unbind("drop");
        $(`.${style.addnew}`).unbind("dragleave");
    }

    render() {
        const { uploads } = this.state;
        const { id, multiple } = this.props;
        return (
            <React.Fragment>
                <input type="file" name={id} className={style.fileInput} id={id} onChange={this.onChangeHandler} multiple={multiple} />
                {
                    uploads.length > 0 && uploads.map((item, i) => (
                        <UploadFile {...this.props} key={i} file={item} />
                    ))
                }
            </React.Fragment>
        );
    }

    onChangeHandler = (e) => {
        const { multiple } = this.props;
        if (e.target.value != "") {
            let files = e.target.files;
            $.each(files, (i, item) => {
                this.setState(prevState => ({
                    uploads: multiple === true ? [...prevState.uploads, item] : [item]
                }))
            });
            e.target.value = "";
        }
    }

    onDropHandler = (files) => {
        const { multiple } = this.props;
        $.each(files, (i, item) => {
            this.setState(prevState => ({
                uploads: multiple === true ? [...prevState.uploads, item] : [item]
            }))
        });
    }

    static propTypes = {
        id: PropTypes.string,
        uploadlink: PropTypes.string,
        format: PropTypes.array,
        sizelimit: PropTypes.number,
        multiple: PropTypes.bool
    };

    static defaultProps = {
        id: 'upload-file',
        uploadlink: API_URL + "/media/create",
        formats: ['jpg', 'jpeg', 'png'],
        sizelimit: 20,
        multiple: true
    };
};

export default Upload;