import React from 'react';
import { toast } from 'react-toastify';
import { get } from 'lodash';
import Store from "../Store";
import { getAccessToken, decrypt } from '../Utils';
import style from "./style.css";

class UploadFile extends React.Component {
    constructor(props) {
        super(props);
        this.defaultState = {
            width: 140,
            name: props.file.name,
            remove: false,
            valid: false,
            progress: 0,
            success: false,
            data: {},
            complete: false,
            onRemoveHandler: this.onRemoveHandler
        }
        this.state = this.defaultState;
        this.timer = null;
    }

    componentDidMount() {
        this.upload();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.file !== this.props.file) {
            this.upload();
        }
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    render() {
        const { progress, remove, valid } = this.state;
        const { renderItem } = this.props;

        if (renderItem) {
            return (
                <this.props.renderItem {...this.state} />
            )
        }

        if (remove) {
            return null;
        }

        return (
            <div className={`${style.media}${valid ? "" : " " + style.invalid}`}>
                <div className={style.mediaContent}>
                    <div className={style.progress}>
                        <div className={style.status} style={{ width: progress + "%" }}></div>
                    </div>
                    <div className={style.filename}>{this.props.file.name}</div>
                    <a className={style.remove} onClick={this.onRemoveHandler}>
                        <span>&#215;</span>
                    </a>
                </div>
            </div>
        );
    }

    onRemoveHandler = () => {
        this.setState({
            remove: true
        }, () => {
            if (this.props.onRemove) {
                this.props.onRemove(this.state.data);
            }
        })
    }

    uploadError = (message) => {
        if (message != "") {
            this.setState({
                valid: false
            })
            if (message) {
                toast.error(message);
            }
            this.timer = setTimeout(() => {
                this.setState({
                    remove: true
                })
            }, 5000)
        }
    }

    uploadSuccess = (response) => {
        const status = get(response, "status", false);
        const data = get(response, "data", false);

        if (data && status) {
            if (status === 200) {
                this.setState({
                    success: true,
                    data: data,
                    complete: true
                });
                if (this.props.onComplete) {
                    this.props.onComplete(data, this.onRemoveHandler);
                }
            }
            else {
                this.uploadError(data.reason);
            }
        }
        else
            this.uploadError("Error in uploading file. Please try later.");
    }

    uploadProgress = (completed) => {
        this.setState({
            progress: completed
        })
    }

    upload = () => {
        const { file, uploadlink, modelId } = this.props;
        const validation = this.validate(file);
        let comp = this;

        let keys = Store.getState().credentials.keys;
        if (validation.valid) {
            this.setState({
                ...this.defaultState,
                valid: true
            });

            //Send file
            var fd = new FormData;
            fd.append('file', file);
            if (modelId) {
                fd.append('id', modelId);
            }
            $.ajax({
                url: uploadlink,
                type: 'POST',
                dataType: 'json',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", "Bearer " + getAccessToken());
                    request.setRequestHeader("X-Api-Key", keys.public);
                },
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    myXhr.addEventListener('progress', function (e) { }, false);
                    if (myXhr.upload) {
                        myXhr.upload.onprogress = function (e) {
                            var completed = 0;
                            if (e.lengthComputable) {
                                var done = e.position || e.loaded,
                                    total = e.totalSize || e.total;
                                completed = Math.round(Math.floor(done / total * 1000) / 10);
                            }
                            comp.uploadProgress(completed);
                        }
                    }
                    return myXhr;
                },
                success: async (data) => {
                    let d = await decrypt(data, keys);
                    comp.uploadSuccess(d);
                    // let d = await decrypt(get(data, "message"), keys);
                    // comp.uploadSuccess(d);
                },
                error: function (jqXHR, textStatus, responseText) {
                    comp.uploadError(responseText);
                },
                data: fd,
                cache: false,
                contentType: false,
                processData: false
            });
        }
        else {
            this.uploadError(validation.reason);
        }
    }

    validate = (file) => {
        const { formats, sizelimit } = this.props;
        const ext = this.extension(file.name);
        let fileAttr = {
            name: file.name,
            orgname: file.name,
            size: file.size,
            fa: ''
        }
        let data = {};

        if (formats != "*" && $.inArray(ext, formats) == -1) {
            data = {
                valid: false,
                reason: file.name + ' is invalid. Please upload a valid file',
                file: fileAttr
            }
        }
        else if (file.size > sizelimit * 1024 * 1024) {
            data = {
                valid: false,
                reason: 'Please upload a file of size less than ' + sizelimit + 'MB',
                file: fileAttr
            }
        }
        else {
            data = {
                valid: true,
                reason: "Success",
                file: fileAttr
            }
        }
        this.setState({
            data: fileAttr
        })
        return data;
    }

    extension = (name) => {
        var ext = name.substr((name.lastIndexOf('.') + 1));
        return ext.toLowerCase();
    }
};

export default UploadFile;