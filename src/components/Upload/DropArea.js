import React from "react";

class DropArea extends React.Component {
    render() {
        return (
            <div className="drag-drop-overlay">
                <div className="bg"></div>
                <div className="receiver-drag-drop">
                    <div className="circle scale"></div>
                    <div className="drop-icon drop-icon1"></div>
                    <div className="drop-icon drop-icon2"></div>
                    <div className="drop-icon drop-icon3"></div>
                    <div className="drop-icon drop-icon4"></div>
                    <div className="drop-icon drop-icon5"></div>
                    <div className="text">
                        <div className="title">Incoming!</div>
                        <p>Drop your files to instantly upload them</p>
                    </div>
                </div>
            </div>
        );
    };
};

export default DropArea;