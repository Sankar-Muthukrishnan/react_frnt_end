import React from 'react';
import style from '~/assets/css/page.css';
import video from '~/assets/images/video.png';
import audio from '~/assets/images/audio.png';
import pdf from '~/assets/images/pdf.png';
import Spinner from '../Spinner';

export default class Background extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            opacity: 0
        };
        this.timer = null;
    }

    componentDidMount() {
        this.loadImage(this.generateUrl());
    }

    componentDidUpdate(prevProps) {
        if (prevProps.name !== this.props.name && this.props.popup === true) {
            this.setState({
                loading: true,
                opacity: 0
            });
            this.loadImage(this.generateUrl());
        }
    }

    generateUrl() {
    
        const { prefix, type, name, orgname, extension, size } = this.props;
        const s = extension == 'jpg' ||  extension == 'jpeg' || extension == 'png' ? size : 'uploads';
        let url = '';
        if (type == 'video') {
            url = video;
        } else if (type == 'audio') {
            url = audio;
        } else if (type == 'pdf' || type == 'word') {
            url = pdf;
        } else {
            url = prefix + '/' + type + '/' + s + '/' + name;
        }
        return url;
    }

    loadImage = (src) => {
        let com = this;
        let image = new Image();
        image.src = src;
        if (image.complete || image.readystate === 4) {
            com.showImage();
        }
        else {
            image.onload = function () {
                com.showImage();
            };
            image.onerror = function () {
                //error message
            };
        }
    }

    showImage = () => {
        this.setState({
            loading: false
        });
        this.timer = setTimeout(() => {
            this.setState({
                opacity: 1
            });
        }, 100);
    }

    content = (data) => {
        const { prefix, type, name, orgname, extension, size, image } = this.props;
        const { loading, opacity } = this.state;
        let url = this.generateUrl();
        const text = type == 'video' || type == 'audio' || type == 'word' || type == 'pdf' ? <div className={style.fileName}>{orgname}</div> : '';

        return (
            <>
                {
                    !loading && (
                        <>
                            <div className={`${style.bsz}`}>
                                <div className={`${style.bgimage}`} style={{ backgroundImage: `url(${!loading ? url : ""})`, opacity: opacity }}></div>
                                <img src={!loading ? url : ""} alt={this.props.alt} />
                            </div>
                            {this.props.popup !== true && text}
                        </>
                    )
                }
                {
                    loading && (
                        <Spinner color="#000" />
                    )
                }
            </>
        );
    }

    render() {
        return (
            <>{this.content()}</>
        );
    }
}