import React from 'react';

import style from '~/assets/css/page.css';

class CustomBackground extends React.Component {
    static defaultProps = {
        image: '',
        alt: 'image'
    };

    render() {
        const { image, alt } = this.props;
        return (
            <div className={`${style.bsz}`}>
                <div className={`${style.bgimage}`} style={{ backgroundImage: `url(${image})` }}></div>
                {image && <img src={image} alt={alt} />}
            </div>
        );
    }
}

export default CustomBackground;