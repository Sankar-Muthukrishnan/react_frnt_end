import { toast } from 'react-toastify';
import style from '~/assets/css/page.css';


export const setAccessToken = (t, name) => {
    if (t) {
        window.localStorage.setItem(name, t);
    }
}

export const getAccessToken = (type) => {
    let val = type ? type : 'access_token';
    const authToken = window.localStorage.getItem(val);
    if (authToken != "" && authToken != undefined) {
        return authToken;
    }
    return false;
}

export const isLoggedIn = () => {
    let j = getAccessToken('access_token');
    if (j) {
        return true;
    }
    return false;
}

export const encrypt = async (data, keys) => {
    if (MODE !== "dev") {
        let nonce = await sodium.randombytes_buf(24);
        let nonce_hex = await sodium.sodium_bin2hex(nonce);
        let encrypted = await sodium.crypto_box(JSON.stringify(data), nonce, keys.secret, keys.shared_public);
        let ciphertext = await sodium.sodium_bin2hex(encrypted);
        return { message: nonce_hex + ciphertext }
    }
    else {
        return data;
    }
}

export const decrypt = async (response, keys) => {
    if (MODE !== "dev") {
        let ciphertext = get(response, "message");
        let cipher = await sodium.sodium_hex2bin(ciphertext.substring(48));
        let nonce = await sodium.sodium_hex2bin(ciphertext.substring(0, 48));
        let decrypted = await sodium.crypto_box_open(cipher, nonce, keys.secret, keys.shared_public);
        return JSON.parse(decrypted.toString());
    }
    else {
        return response;
    }
}

export const showToast = (errors) => {
    toast(errors, {
        position: "top-right",
        autoClose: 5000,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
    });
}

export const toggle = (className) => {
    $(`.${style.tableHead}`).toggleClass(`${style.show}`);
    $(`.` + className).slideToggle();
}


export const nl2br = (str, is_xhtml) => {
    if (!str) {
        return "";
    }
    var str = str.replace(/^\s+|\s+$/g, '');
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

export const formatDateTime = (d, format) => {
    if (!format)
        format = "MMM DD, Y [at] hh:mma";
    return moment(d * 1000).format(format);
}

export const sectionList = () => {
    return {
        "user": "Admin",
    }
}

export const actionList = () => {
    return {
        "created": "Create",
        "updated": "Update",
        "deleted": "Delete",
        "restored": "Restore"
    }
}

export const actionLinkType = () => {
    return {
        "upload": "File Upload"
    }
}

export function makeHeightByRow(elem) {
    $(elem).matchHeight({
        property: 'height',
        byRow: true
    });
}

export const addClass = (type, element, classname) => {
    var root;

    if (type === "tag")
        root = document.getElementsByTagName(element)[0];

    if (type === "class")
        root = document.getElementsByClassName(element)[0];

    if (type === "id")
        root = document.getElementById(element);

    root.classList.add(classname);
}

export const removeClass = (type, element, classname) => {
    let root = "";

    if (type === "tag")
        root = document.getElementsByTagName(element);

    if (type === "class")
        root = document.getElementsByClassName(element);

    if (type === "id")
        root = document.getElementById(element);

    if (root.length <= 2) {
        for (let i = 0; i < root.length; i++) {
            root[i].classList.remove(classname);
        }
    }
    else {
        root.classList.remove(classname);
    }
}

export const getList = (list, deletePage) => {
    let changeList = list;
    if (deletePage) {
        changeList = list.filter(l => l.deleted === 1);
    } else {
        changeList = list.filter(l => l.deleted === 0);
    }
    return changeList;
}

export const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}