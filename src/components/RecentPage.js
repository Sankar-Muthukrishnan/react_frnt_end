import React from 'react';
import style from '~/assets/css/page.css';
import IconButton from './iconButton';
import { pin } from './Utils';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { connect } from 'react-redux';

class Recentpage extends React.Component {
    render() {
        const { recentpages } = this.props;
        return (
                <div>
                    <div className={`${style.recentPage} ${style.commonWrapper}`}>
                        <div className={style.relative}>
                            <div className={`${style.col} ${style.tileTitleAr}`}>
                                <span className={style.tileTitle}>Recent page</span>
                            </div>
                        </div>
                        <Droppable droppableId="droppable-3" type="recentPage" >
                            {(provided, snapshot) => (
                                <div ref={provided.innerRef} style={{ backgroundColor: snapshot.isDraggingOver ? 'aliceblue' : 'transparent' }} {...provided.droppableProps}>
                                    {recentpages.map((pa, n) => {
                                        let id = 'PageId' + n;
                                        return (
                                            <Draggable draggableId={id} index={n} key={n}>
                                                {(provided, snapshot) => (
                                                    <div className={`${style.relative} ${style.recentPageBlock}`} id={'page'+pa.id} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                        <div className={`${style.col} ${style.tileActionAr}`}>
                                                            <IconButton icon={`push_pin`} classes={`${style.tileIcon} ${style.pinItem}`} clickdo={() => this.props.pinnable('page'+pa.id)} />
                                                            <IconButton icon={true} icon={`refresh`} classes={`${style.tileIcon}`} />
                                                            <IconButton icon={true} icon={`clear`} classes={`${style.tileIcon} ${style.delItem}`} />
                                                        </div>
                                                        <div className={`${style.recentPageList} ${style.col}`}>
                                                            <a href="" className={style.recentPageLink} target="_blank">{pa.title}</a>
                                                        </div>
                                                    </div>
                                                )}
                                            </Draggable>
                                        )

                                    })}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </div>
                </div>
        )
    };
};

export default connect()(Recentpage);