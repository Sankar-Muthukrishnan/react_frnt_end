import React from 'react';
import style from '~/assets/css/page.css';
import phstyle from '~/assets/css/placeholder.css';

export default class FormPlaceholder extends React.Component {
    render() {
        {

            return (
                <div className={`${phstyle.userUpdatePlace}`}>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Expiry Date</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Password</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Link Type</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={`${phstyle.bg} ${phstyle.rectangle} ${style.rectangle} ${style.bg}`}>&nbsp;</div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Title</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Text</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Button Text</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Button Link</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={`${phstyle.bg} ${phstyle.rectangle} ${style.rectangle} ${style.bg}`}>&nbsp;</div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Meta Title</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Meta Description</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.buttons}></div>
                </div>
            );

        }
    };
};