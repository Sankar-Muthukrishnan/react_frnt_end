import React from 'react';
import phstyle from '~/assets/css/placeholder.css';

export default class Placeholder extends React.Component {
    render() {
        const { count } = this.props;
        let list = [];
        for (var i = 0; i < count; i++) {
            list.push(
                <div className={`${phstyle.rowColum} ${phstyle.userListPlace}`} key={i}>
                        <div className={`${phstyle.rectangle} ${phstyle.tOne}`}></div>
                        <div className={`${phstyle.rectangle} ${phstyle.tTwo}`}></div>
                        <div className={`${phstyle.rectangle} ${phstyle.tThree}`}></div>
                        <div className={`${phstyle.rectangle} ${phstyle.tFour}`}></div>
                    </div>
            );
        }
        return (
            <React.Fragment>{list}</React.Fragment>
        )
    };
};


