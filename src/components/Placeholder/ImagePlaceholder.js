import React from 'react';
import style from '~/assets/css/page.css';
import phstyle from '~/assets/css/placeholder.css';

export default class FormPlaceholder extends React.Component {
    render() {
        {
            return (
                <div className={`${phstyle.userUpdatePlace}`}>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Name</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={`${phstyle.row} ${phstyle.buttons}`}>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={`${phstyle.row} ${phstyle.imgeRow} ${phstyle.picture} ${style.picture}`}>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>                    
                </div>
            );

        }
    };
};