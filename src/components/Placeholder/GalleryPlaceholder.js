import React from 'react';
import style from '~/assets/css/page.css';
import phstyle from '~/assets/css/placeholder.css';

export default class Placeholder extends React.Component {
    render() {
        const { count, grid } = this.props;
        let list = [];
        for (var i = 0; i < count; i++) {
            list.push(
                <div className={`${style.col}  ${style.galleryItem}`} key={i}>
                    <div className={`${style.galleryItemImage} ${phstyle.rectangle} ${phstyle.bg}  ${style.pRea}`}>
                        <div className={`${style.mediaSizer}`}>

                        </div>
                    </div>
                    <div className={`${style.galleryLabel}`}><div className={`${phstyle.bg} ${phstyle.rectangle}`}>&nbsp;</div></div>
                </div>
            );
        }
        return (
            <React.Fragment>{list}</React.Fragment>
        )
    };
};


