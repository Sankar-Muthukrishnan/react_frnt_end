import React from 'react';
import phstyle from '~/assets/css/placeholder.css';

export default class FormPlaceholder extends React.Component {
    render() {
        {

            return (
                <div className={`${phstyle.userUpdatePlace}`}>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Firstname</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>surname</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Quote</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Email</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.row}>
                        <div className={phstyle.controlsLabel}>Username</div>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={`${phstyle.row} ${phstyle.imgeRow}`}>
                        <div className={phstyle.inner}>
                            <div></div>
                        </div>
                    </div>
                    <div className={phstyle.buttons}></div>
                </div>
            );

        }
    };
};