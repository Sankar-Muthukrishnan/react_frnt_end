import React from 'react';
import style from '~/assets/css/page.css';
import phstyle from '~/assets/css/placeholder.css';

export default class Placeholder extends React.Component {
    render() {
        const { count, grid } = this.props;
        let list = [];
        for (var i = 0; i < count; i++) {
            list.push(
                <div className={`${phstyle.mediaPlaceholder} ${style.mediaRow}`} key={i}>
                    <div className={`${style.media}`} key={i}>
                        <div className={`${phstyle.rectangle} ${style.rectangle} ${style.bg} ${style.mediaSizer} ${phstyle.bg}`}></div>
                    </div>
                    <div className={`${style.mediaName} ${style.mediaDetail} ${phstyle.mediaDetail}`}>
                        <div className={`${phstyle.bg} ${phstyle.rectangle} ${style.rectangle} ${style.bg}`}>&nbsp;</div>
                        <div className={`${phstyle.bg} ${phstyle.rectangle} ${style.rectangle} ${style.bg} ${style.mediaFourTwenty}`}>&nbsp;</div>
                        <div className={`${phstyle.bg} ${phstyle.rectangle} ${style.rectangle} ${style.bg} ${style.mediaFourTwenty}`}>&nbsp;</div>
                    </div>
                    <div className={`${style.mediaDetail} ${style.mediaMobileFourTwenty} ${phstyle.mediaDetail}`}>
                        <div className={`${phstyle.bg} ${phstyle.rectangle} ${style.rectangle} ${style.bg}`}>&nbsp;</div>
                        <div className={`${phstyle.bg} ${phstyle.rectangle} ${style.rectangle} ${style.bg} ${style.mediaThousand}`}>&nbsp;</div>
                    </div>
                    <div className={`${style.mediaDetail} ${style.mediaMobileThousand} ${phstyle.mediaDetail}`}><div className={`${phstyle.bg} ${phstyle.rectangle}`}>&nbsp;</div></div>
                </div>
            );
        }
        return (
            <React.Fragment>{list}</React.Fragment>
        )
    };
};


