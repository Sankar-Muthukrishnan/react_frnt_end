import React from 'react';
import { connect } from "react-redux";
import { reduxForm, Field } from 'redux-form';
import { Input } from '../Field';
import { addClass, removeClass } from "../Utils";
import { searchItems } from "~/actions";
import style from '../../assets/css/page.css';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    render() {
        return (
            <>
                <div className={style.searchWrap}>
                    <form onSubmit={this.onSubmitHandler}>
                        <span className={`${style.icon} ${style.left} fas fa-search`}></span>
                        <Field name="search" label="false" component={Input} type="text" placeholder="Start typing to find..." />
                        <a type="submit" className={`${style.icon} ${style.right}`} onClick={() => { this.toggleSearch() }}>
                            <i className="fas fa-times"></i>
                        </a>
                    </form>
                </div>
                <a className={`${style.optionLink} ${style.icon}`} onClick={() => { this.toggleSearch() }}>
                    <i className={`fal fa-search ${style.icn}`}></i>
                    <span>Search</span>
                </a>
            </>
        )
    }

    onSubmitHandler = (e) => {
        this.props.handleSubmit(e);
        return false;
    }

    toggleSearch = () => {
        const { open } = this.state;
        const { change } = this.props;
        this.setState({
            open: !open
        }, () => {
            if (open) {
                change("search", "");
                removeClass('tag', 'html', style.searchOpen)
            }
            else {
                addClass('tag', 'html', style.searchOpen);
                setTimeout(() => {
                    $('input[name="search"]').focus();
                }, 500);
            }
        })
    }
}

const mapDispatchToProps = (dispatch, ownprops) => {
    return {
        onSubmit: (data) => { dispatch(searchItems(ownprops.attribute, ownprops.end, data)) },
    }
}

let timeout = null;
export default connect(null, mapDispatchToProps)(reduxForm({
    form: "search",
    onChange: (values, dispatch, props, previousValues) => {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            props.onSubmit(values)
        }, 300);
    }
})(Search));