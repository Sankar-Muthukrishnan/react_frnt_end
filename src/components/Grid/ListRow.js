import React from 'react';
import { get } from 'lodash';
import { Link } from 'react-router-dom';
import style from '~/assets/css/page.css';
import fa from '~/assets/css/font-awesome.css';
import moment from 'moment';
import { formatDateTime, numberWithCommas } from "../Utils";

class ListRow extends React.Component {
    render() {
        const { attribute, item, columns, itemActions } = this.props;
        const { id, deleted } = item;
        return (
            <div className={`${style.tableRow}${deleted === 1 ? " " + style.deletedRow : ""}`}>
                <div className={style.flexRow}>
                    <Link to={`/${attribute}/${id}`} className={`${style.fullRowLink}`}>&nbsp;</Link>
                    {
                        Object.entries(columns).map(([key, value]) => (
                            < div className={style.tableData} data-title={value.column_title || value} key={key}>{this.renderValue(key)}</div>
                        ))
                    }
                </div>
                <div className={`${style.tableData} ${style.tableDataActions}`}>
                    <>
                        {
                            deleted === 0 && (
                                <a onClick={() => itemActions("delete", { ids: [id] })} className={`${style.actionLink}`}>
                                    <i className="fas fa-trash-alt"></i>
                                </a>
                            )
                        }
                        {
                            deleted === 1 && (
                                <>
                                    <a onClick={() => itemActions("restore", { ids: [id] })} className={`${style.actionLink}`}>
                                        <i className="fas fa-recycle"></i>
                                    </a>
                                    <a onClick={this.onHideClick} className={`${style.actionLink}`}>
                                        <i className="fas fa-times"></i>
                                    </a>
                                </>
                            )
                        }
                    </>
                </div>
            </div >
        )
    }

    onHideClick = () => {
        const { item: { id }, itemActions } = this.props;
        let response = confirm("Are you sure you want to permanently delete this record?");
        if (response === true) {
            itemActions("hide", { ids: [id] });
        }
    }

    renderValue = (key) => {
        const { columns, item, attribute } = this.props;
        let type = get(columns, `${key}.type`, "text");
        if (type === "text") {
            return get(item, key, "");
        }
        if (type === "boolean") {
            return get(item, key, 0) == 0 ? "No" : "Yes";
        }
        if (type === "tick") {
            if (get(item, key, 0) == 0) {
                return (
                    <span className={style.bad}>
                        <i className="far fa-times"></i>
                    </span>
                )
            }
            else {
                return (
                    <span className={style.good}>
                        <i className="far fa-check"></i>
                    </span>
                )
            }
        }
        if (type === "approve_status") {
            if (get(item, key, 0) == 0) {
                return (
                    <span className={style.approveStatus}>Pending</span>
                )
            }
            else {
                return (
                    <span className={`${style.approveStatus} ${style.good}`}>Approved</span>
                )
            }
        }
        if (type === "state") {
            if (get(item, key, 0) == 0) {
                return (
                    <span className={style.approveStatus}>Draft</span>
                )
            }
            else {
                return (
                    <span className={`${style.approveStatus} ${style.good}`}>Published</span>
                )
            }
        }
        if (type === "date") {
            return formatDateTime(get(item, `${key}`));
        }
    }
}

export default ListRow;