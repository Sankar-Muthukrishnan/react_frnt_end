import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { get, sumBy } from "lodash";
import { getItems, loadMore, itemActions, sortItems, headerButtons } from '~/actions';
import ListHead from './ListHead';
import ListRow from './ListRow';
import Search from './Search';
import Placeholder from '../Placeholder/Placeholder';
import InfiniteScroll from "../InfiniteScroll";
import style from '~/assets/css/page.css';
import { toggle } from '~/components/Utils';

class Grid extends React.Component {
    componentDidMount() {
        const { getItems, attribute, limit = 20, headerButtons, buttons = false, end } = this.props;
        headerButtons(
            <>
                {buttons !== false && buttons}
                <Search attribute={attribute} end={end} />
            </>
        );
        getItems({ 'limit': limit });
    }

    toggle = () => {
        toggle(`${style.gridTable}`);
    }

    render() {
        const { attribute, action, title = "", columns = {}, list = [], fetching, end, order = 'asc', sortItems, itemActions, total, limit = 20 } = this.props;
        let inactive = sumBy(list, "deleted");
        return (
            <div className={style.tableRowMain}>
                <div className={style.tableHeadMain} onClick={() => this.toggle()}>
                    <h1 className={style.sectionTitle}>
                        <span>{title} </span>
                        <span>(<b>{action === "search" || action === "hide" ? list.length : total}</b>)</span>
                    </h1>
                    {
                        inactive > 0 && (
                            <div className={style.listCounts}><span><b>{list.length - sumBy(list, "deleted")}</b> Active</span><span><b>{inactive}</b> Inactive</span></div>
                        )
                    }
                </div>
                <div className={`${style.gridTable}`}>
                    <div className={style.tableHead}>
                        <ListHead columns={columns} end={end} order={order} sortItems={sortItems} />
                    </div>
                    <div className={style.tableBody}>
                        {
                            list && list.length > 0 && list.map((item) => (
                                <ListRow attribute={attribute} columns={columns} item={item} itemActions={itemActions} key={item.id} />
                            ))
                        }
                        {
                            fetching === false && list && list.length === 0 && (
                                <div className={`${style.noDataFound} ${style.noDataPadding} ${style.activity}`}>No {attribute}s found</div>
                            )
                        }
                        {
                            fetching === true && action === "fetching" && (
                                <Placeholder count={10} column={Object.keys(columns).length} />
                            )
                        }
                    </div>
                </div>
                <InfiniteScroll onLoadMore={() => { this.props.loadMore(list.length, limit) }} fetching={fetching} end={end} />
            </div>
        )
    }
}

const mapStateToProps = (state, ownprops) => {
    return {
        ...get(state.list, ownprops.attribute)
    }
}

const mapDispatchToProps = (dispatch, ownprops) => {
    let attribute = ownprops.attribute;
    return {
        headerButtons: (data) => { dispatch(headerButtons(data)) },
        getItems: (data) => { dispatch(getItems(attribute, data)) },
        sortItems: (end, data) => { dispatch(sortItems(attribute, end, data)) },
        loadMore: (count, limit) => { dispatch(loadMore(attribute, count, limit)) },
        itemActions: (action, data) => { dispatch(itemActions(attribute, action, data)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);