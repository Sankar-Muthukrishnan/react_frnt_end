import React from 'react';
import { get } from 'lodash';
import { connect } from "react-redux";

import ListView from "./ListView";
import { getItem, setupItem, headerButtons } from '../../actions';
import style from '../../assets/css/page.css';

class View extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: get(props, "match.params.id", false)
        }
    }

    componentDidMount() {
        const { list, headerButtons } = this.props;
        const { id } = this.state;
        headerButtons();
        if (id) {
            if (list && list.length > 0) {
                let data = list.find(item => item.id === id);
                if (data) {
                    this.props.setupItem(data);
                }
                else {
                    this.props.getItem(id);
                }
            }
            else {
                this.props.getItem(id);
            }
        }
    }

    render() {
        const { title = "", item, fetching, columns } = this.props;
        return (
            <>
                {
                    title !== "" && (
                        <div className={style.titleSection}>
                            <h1 className={style.sectionTitle}>View {title}</h1>
                        </div>
                    )
                }
                <div className={`${style.gridView}`}>
                    {
                        fetching === false && columns && Object.entries(columns).map(([key, value]) => (
                            <ListView item={item} attribute={key} value={value} columns={columns} key={key} />
                        ))
                    }
                </div>
            </>
        )
    }
}

const mapStateToProps = (state, ownprops) => {
    const { attribute } = ownprops;
    return {
        ...get(state.list, attribute),
        item: get(state.list, attribute + ".item")
    }
}

const mapDispatchToProps = (dispatch, ownprops) => {
    return {
        headerButtons: (data) => { dispatch(headerButtons(data)) },
        getItem: (id) => { dispatch(getItem(ownprops.attribute, id)) },
        setupItem: (data) => { dispatch(setupItem(ownprops.attribute, data)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View);