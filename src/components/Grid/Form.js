import React from 'react';
import { get } from 'lodash';
import { toast } from 'react-toastify';
import { connect } from "react-redux";
import { reduxForm } from 'redux-form';
import { getItem, setupItem, submitItem, headerButtons } from '../../actions';
import { Button } from "../Field";
import style from '~/assets/css/page.css';
import { Link } from "react-router-dom";

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: get(props, "match.params.id", false)
        }
    }

    componentDidMount() {
        const { list, showBtn = true } = this.props;
        const { id } = this.state;
        if (id) {
            if (list && list.length > 0) {
                let data = list.find(item => item.id === id);
                if (data) {
                    this.props.setupItem(data);
                }
                else {
                    this.props.getItem(id);
                }
            }
            else {
                this.props.getItem(id);
            }
        }
        if (!showBtn) {
            this.props.headerButtons(
                <>
                    <a className={style.optionLink} onClick={this.props.handleSubmit(this.props.onSubmit)}><i className={`far fa-save ${style.icn}`}></i><span>Save</span></a>
                </>
            );
        }

    }

    componentDidUpdate(prevProps) {
        const { status, action, reset, title, history, attribute } = this.props;
        if (prevProps.status !== status && status == 200) {
            if (action === "create") {
                reset();
            }
            history.push(`/${attribute}`)
            toast.success(`${title} was ${action}d successfully`);
        }
    }

    render() {
        const { title = "", children, handleSubmit, onSubmit, invalid, fetching, showBtn = true } = this.props;
        const { id } = this.state;
        return (
            <>
                <div className={style.titleSection}>
                    <h1 className={style.sectionTitle}>{id ? "Update" : "Create"} {title}</h1>
                </div>
                <div className={`${style.tableForm}`}>
                    {!showBtn &&
                        <form>
                            {children}
                        </form>
                    }
                    {showBtn &&
                        <form onSubmit={handleSubmit(onSubmit)}>
                            {children}
                            <div className={`${style.formGroup} ${style.alignRight}`}>
                                <Button classes={`${style.btnPrimary} ${style.btnCons}`} submit={true} disabled={invalid} fetching={fetching} label={"Save"} />
                            </div>
                        </form>
                    }
                </div>
            </>
        )
    }
}

const mapStateToProps = (state, ownprops) => {
    return {
        ...get(state.list, ownprops.attribute),
        initialValues: {
            ...get(state.list, ownprops.attribute + ".item")
        }
    }
}

const mapDispatchToProps = (dispatch, ownprops) => {
    return {
        headerButtons: (data) => { dispatch(headerButtons(data)) },
        getItem: (id) => { dispatch(getItem(ownprops.attribute, id)) },
        setupItem: (data) => { dispatch(setupItem(ownprops.attribute, data)) },
        onSubmit: (data) => { dispatch(submitItem(ownprops.attribute, data)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'form',
    enableReinitialize: true
})(Form));