import React from 'react';
import style from '~/assets/css/page.css';
import fa from '~/assets/css/font-awesome.css';

class ListHead extends React.Component {
    render() {
        const { columns, order: { sort_order } } = this.props;
        return (
            <div className={style.tableRow}>
                <div className={style.flexRow}>
                    {
                        Object.entries(columns).map(([key, item]) => (
                            <a className={style.tableData} key={key} onClick={() => this.onClickHandler(key)}>
                                <span>{this.renderTitle(item)}</span>
                                <span className={style.sortOrder} key={`${key}_${sort_order}`}>
                                    {this.renderSortIcon(key)}
                                </span>
                            </a>
                        ))
                    }
                </div>
                <div className={`${style.tableData} ${style.tableDataActions}`}>
                    <div className={style.tableData}></div>
                </div>
            </div>
        )
    }

    renderTitle = (item) => {
        return item.column_title || item;
    }

    renderSortIcon = (key) => {
        const { order: { field, sort_order } } = this.props;
        if (field === key) {
            if (sort_order === "asc") {
                return (
                    <span>
                        <i className="fas fa-caret-up"></i>
                    </span>
                )
            }
            return (
                <span>
                    <i className="fas fa-caret-down"></i>
                </span>
            )
        }
        return null;
    }

    onClickHandler = (key) => {
        const { sortItems, end, order: { field, sort_order } } = this.props;
        if (field === key && sort_order === "asc") {
            sortItems(end, {
                field: key,
                sort_order: "desc"
            })
        }
        else if (field === key && sort_order === "desc") {
            sortItems(end, {
                field: "id",
                sort_order: "desc"
            })
        }
        else {
            sortItems(end, {
                field: key,
                sort_order: "asc"
            })
        }
    }
}

export default ListHead;