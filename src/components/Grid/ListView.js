import React from 'react';
import { renderValue } from "./ListValue";
import style from '../../assets/css/page.css';

export default class ListView extends React.Component {
    render() {
        const { attribute, value, item, columns } = this.props;
        console.log(this.props);
        return (
            <div className={`${style.viewRow}`}>
                <div className={`${style.viewLabel} ${style.col}`}>{value.column_title || value}</div>
                <div className={`${style.viewValue} ${style.col}`}>{renderValue(attribute, item, columns)}</div>
            </div>
        )
    }
}