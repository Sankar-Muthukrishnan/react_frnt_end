import React from 'react';
import { get } from 'lodash';
import Tooltip from '../Tooltip';
import { formatDateTime, numberWithCommas } from "../Utils";
import style from '../../assets/css/page.css';
import { object } from 'prop-types';

export const renderValue = (key, item, columns) => {
    let type = get(columns, `${key}.type`, "text");
    if (key === "created_at" || key === "updated_at") {
        return formatDateTime(get(item, `${key}`));
    }
    if (type === "date") {
        return formatDateTime(get(item, `${key}`));
    }
    if (type === "number") {
        return numberWithCommas(get(item, `${key}`));
    }
    if (type === "text") {
        return get(item, key, "");
    }
    if (type === "boolean") {
        return get(item, key, 0) == 0 ? "No" : "Yes";
    }
    if (type === "tick") {
        if (get(item, key, 0) == 0) {
            return (
                <span className={style.bad}>
                    <i className="far fa-times"></i>
                </span>
            )
        }
        else {
            return (
                <span className={style.good}>
                    <i className="far fa-check"></i>
                </span>
            )
        }
    }
    if (type === "approve_status") {
        if (get(item, key, 0) == 0) {
            return (
                <span className={style.approveStatus}>Pending</span>
            )
        }
        else if (get(item, key, 0) == -1) {
            return (
                <span className={`${style.approveStatus} ${style.bad}`}>Rejected</span>
            )
        }
        else {
            return (
                <span className={`${style.approveStatus} ${style.good}`}>Approved</span>
            )
        }
    }
    if (type === "page_state") {
        if (get(item, key, 0) === 0) {
            return (
                <span className={style.approveStatus}>{item.state_text}</span>
            )
        }
        else if (get(item, "publish_date", 0) !== 0 && item.publish_date >= moment().unix()) {
            return (
                <span className={`${style.approveStatus} ${style.bad}`}>{item.state_text}</span>
            )
        }
        else {
            return (
                <span className={`${style.approveStatus} ${style.good}`}>{item.state_text}</span>
            )
        }
    }

    if (type === "fas-icon") {
        return <span className={`fas fa-${get(item, 'icon')} ${style.gridIcon}`}></span>
    }
    if (type === "data-object") {
        return get(item, `${key}`, "");
    }
}