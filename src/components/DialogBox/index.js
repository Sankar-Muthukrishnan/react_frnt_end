import React from 'react';
import ModalBase from './ModalBase';
import style from '~/assets/css/page.css';

export default class DialogBox extends React.Component {
    static defaultProps = {
        title: "Kgis",
        type: "",
        content: "",
        buttons: "",
        dialogClass: "",
        onClose: false
    }

    constructor(props) {
        super(props);
        this.state = {
            openClass: "",
            closeClass: ""
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                openClass: " " + style.dialogOpen
            })
        }, 50)
        $(document).on('keyup', this.keyUp);
    }

    keyUp = (e) => {
        if (e.key === "Escape") {
            this.onCloseHandler();
        }
    }

    onCloseHandler = () => {
        this.setState({
            closeClass: " " + style.dialogRemove
        }, () => {
            setTimeout(() => {
                $(document).off('keyup', this.keyUp);
                this.props.onClose();
            }, 1250)
        })
    }

    render() {
        const { title, content, buttons, dialogClass, children, type, onClose } = this.props;
        const { openClass, closeClass } = this.state;
        if (type === "fullscreen") {
            return (
                <ModalBase>
                    <div className={`${style.dialogBoxOverlay}${dialogClass ? " " + dialogClass : ""}${openClass}${closeClass}`}>
                        {content && content(this.onCloseHandler)}
                        {children && <div className={style.dialogContent}>{children}</div>}
                    </div>
                </ModalBase>
            )
        }

        return (
            <ModalBase>
                <div className={`${style.dialogBoxOverlay}${dialogClass ? " " + dialogClass : ""}${openClass}${closeClass}`}>
                    <div className={style.dialogBox}>
                        <div className={style.dialogPosition}>
                            <div className={style.dialogHeader}>
                                <h5 className={style.dialogTitle}>{title}</h5>
                                {   
                                    onClose && <a className={style.dialogClose} onClick={this.onCloseHandler}>&#215;</a>
                                }
                            </div>
                            {content && <div className={style.dialogContent}>{content(this.onCloseHandler)}</div>}
                            {children && <div className={style.dialogContent}>{children}</div>}
                            <div className={style.dialogFooter}>{buttons}</div>
                        </div>
                    </div>
                </div>
            </ModalBase>
        );
    }
}