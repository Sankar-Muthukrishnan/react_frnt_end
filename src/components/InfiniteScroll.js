import React from 'react';

export default class InfiniteScroll extends React.Component {
    static defaultProps = {
        el: window,
        end: false,
        fetching: false,
    };

    constructor(props) {
        super(props);
        this.timer = this.winHei = this.offsetPercent = 0;
    }

    componentDidMount() {
        const { el } = this.props;
        this.timer = setTimeout(() => {
            this.resizeWindow();
            $(window).on('resize', this.resizeWindow);
            $(el).on('scroll', this.loadOnScroll);
        }, 100)
    }

    componentDidUpdate(prevProps) {
        const { el } = this.props;
        if (el !== prevProps.el) {
            this.resizeWindow();
            $(prevProps.el).off('scroll', this.loadOnScroll);
            $(el).on('scroll', this.loadOnScroll);
        }
    }

    componentWillUnmount() {
        const { el } = this.props;
        $(window).off('resize', this.resizeWindow);
        $(el).off('scroll', this.loadOnScroll);
        clearTimeout(this.timer);
    }

    render() {
        return null;
    }

    loadOnScroll = () => {
        const { end, fetching } = this.props;
        if (end === false && fetching === false && this.crossedScrollThreshold()) {
            this.props.onLoadMore();
        }
    }

    resizeWindow = () => {
        const { el } = this.props;
        this.winHei = $(el).outerHeight();
        this.offsetPercent = Math.round(this.winHei * 0.30);
    }

    crossedScrollThreshold = () => {
        const { el } = this.props;
        let st = $(el).scrollTop() + this.winHei + this.offsetPercent;
        let sh = el == window ? document.documentElement.scrollHeight : $(el)[0].scrollHeight;
        return st >= sh;
    }
}