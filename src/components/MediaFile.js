import React from 'react';
import Spinner from './Spinner';
import { get } from 'lodash';
import style from '~/assets/css/page.css';
import video from '~/assets/images/video.png';
import audio from '~/assets/images/audio.png';
import Background from '~/components/Background';

export default class MediaFile extends React.Component {
    render() {
        const { remove, valid, success, data, complete, progress, onRemoveHandler } = this.props;
        if (remove || !valid) {
            return null;
        }
        return (
            <div className={`${style.col} ${style.item} ${style.loaderItemSingle}`}>
                {
                    success === true && data && (
                        <Background image="yes" prefix={get(data,'prefix')} type={get(data,'type')} orgname={get(data,'orgname')} name={get(data,'name')} extension={get(data,'extension')} size="uploads" />
                    )
                }
                {
                    complete === false && (
                        <Spinner color="#000" />
                    )
                }
            </div>
        )
    }
}
      