import React from 'react';
import { Link } from "react-router-dom";
import style from '~/assets/css/page.css';

class NormalLink extends React.Component {
    render() {
        const { children = "", link = "", classes = "", onClick = "", disabled = false, download = false, target = false } = this.props;
        let mailtoCondt = (link.indexOf("mailto:") !== -1);
        let telCondt = (link.indexOf("tel:") !== -1);
        let httpCondt = (link.indexOf("http") !== -1);
        let hashCond = (link.indexOf("#") == 0);

        if (disabled) {
            return (
                <div className={`${classes} ${style.disabledLink}`}>
                    {children}
                </div>
            );
        }

        if (onClick) {
            return (
                <a className={`${classes}`} onClick={onClick}>
                    {children}
                </a>
            );
        }

        if (link == "") {
            return (
                <div className={`${classes}`}>
                    {children}
                </div>
            );
        }

        if (httpCondt || mailtoCondt || telCondt) {
            return (
                <a className={`${classes}`} href={link} target={httpCondt ? "_blank" : ""} download={download}>
                    {children}
                </a>
            );
        }

        if (hashCond) {
            return (
                <a className={`${classes}`} data-scroll={link}>
                    {children}
                </a>
            );
        }

        if (target) {
            return (
                <a className={`${classes}`} href={link} target={target}>
                    {children}
                </a>
            );
        }


        return (
            <Link className={`${classes}`} to={link}>
                {children}
            </Link>
        );
    }
};

export default NormalLink;