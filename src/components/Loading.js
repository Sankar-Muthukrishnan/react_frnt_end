import React from 'react';
import style from '~/assets/css/page.css';

class Loading extends React.Component {

    static defaultProps = {
        topMargin: false
    }

    render() {
        const { topMargin } = this.props;
        
        return (
            <div className={`${style.loading_wrap}${topMargin === true ? " " + style.topMargin : ''}`}></div>
        );
    }
};

export default Loading;