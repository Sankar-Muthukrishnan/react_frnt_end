import React from 'react';
import style from '~/assets/css/page.css';

export default class Spinner extends React.Component {
    static defaultProps = {
        color: '#8C8C8C',
        size: 'small'
    }

    getSize = () => {
        const { size } = this.props;
        if (size === "large") {
            return {
                width: 36,
                height: 36
            }
        }

        return {
            width: 20,
            height: 20
        }
    }

    render() {
        const { color } = this.props;
        return (
            <div className={style.spinner} style={this.getSize()}>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
                <div className={style.spinnerBlade} style={{ backgroundColor: color }}></div>
            </div>
        );
    }
}