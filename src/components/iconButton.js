import React from 'react'
import style from '~/assets/css/page.css';
import { Link } from 'react-router-dom';

export default class IconButton extends React.Component {
    static defaultProps = {
        label: "",
        target: '_self'
    }

    render() {
        const { classes, label, icon, link, target, clickdo } = this.props;
        return (
            <React.Fragment>
                {link &&
                    <Link to={link} className={`${classes ? " " + classes : ""}`} target={target}>
                        <span dangerouslySetInnerHTML={{ __html: label }}></span>
                        <i className={`material-icons`}>{icon}</i>
                    </Link>
                }
                
                {!link &&
                <a className={`${classes ? " " + classes : ""}`} target={target} onClick={clickdo}>
                    <span dangerouslySetInnerHTML={{ __html: label }}></span>
                    <i className={`material-icons`}>{icon}</i>
                </a>
                }
            </React.Fragment>
        );
    };
};