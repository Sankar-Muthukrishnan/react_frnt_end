import React from 'react';

class CheckboxSvg extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let circleStyle = {
            display: this.props.isSelected ? "block" : "none"
        };

        return (
            <svg
                fill="rgba(255, 255, 255, 1)"
                height={this.props.height} viewBox="0 0 24 24"
                width={this.props.width}
                xmlns="http://www.w3.org/2000/svg">

                <radialGradient
                    id="shadow"
                    cx="38"
                    cy="95.488"
                    r="10.488"
                    gradientTransform="matrix(1 0 0 -1 -26 109)"
                    gradientUnits="userSpaceOnUse">
                    <stop
                        offset=".832"
                        stopColor="#010101">
                    </stop>
                    <stop
                        offset="1"
                        stopColor="#010101"
                        stopOpacity="0">
                    </stop>
                </radialGradient>

                <circle
                    style={circleStyle}
                    opacity=".26"
                    fill="url(#shadow)"
                    cx="12" cy="13.512"
                    r="10.488">
                </circle>
                <circle
                    style={circleStyle}
                    fill="#FFF"
                    cx="12"
                    cy="12.2"
                    r="8.292">
                </circle>
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z" />
            </svg>
        );
    }
};

export default CheckboxSvg;