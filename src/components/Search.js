import React from 'react';
import style from '~/assets/css/page.css';
import { Button, Input } from '~/components/Field';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { required } from 'redux-form-validators';

class Search extends React.Component {

    onsubmit = (data) => {
        this.props.history.push('/search/'+ data.search_overall);
        this.props.onclose();
    }
    componentDidMount() {
        setTimeout(() => {
            $('input[name="search_overall"]').focus();
        }, 500);
    }

    render() {
        const { onclose, invalid, fetching, handleSubmit } = this.props;
        let searchList = [...Array(5)];
        return (
            <div className={style.searchBlog}>
                <a className={`${style.close_icon} material-icons`} onClick={() => onclose()}>close</a>
                <div className={style.searchBlogWrap}>
                    <div className={`${style.relative}`}>
                        <form onSubmit={handleSubmit(this.onsubmit)}>
                            <div className={`${style.searchFieldArea}`}>
                                <Field name="search_overall" component={Input}  validate={[required()]} placeholder="Type keyword here.." onChange={this.onChangeHandler} />
                            </div>
                            <div className={`${style.searchButtonArea}`}>
                                <Button classes={`${style.btnCons}`} submit={true} disabled={invalid} fetching={fetching} label={"SEARCH"} />
                            </div>
                        </form>
                    </div>
                    <div className={`${style.searchWrapResult}`}>
                            {searchList.map((v, i) => {
                                return (
                                    <div className={`${style.searchRow}`} key={i}>
                                        <Link to={`${PREFIX_URL}` + `/`}>Searched item</Link>
                                        <div className={style.searchedType}>Type</div>
                                    </div>
                                )
                            })}

                        </div>
                </div>

            </div>
        );
    };
};

export default (reduxForm({
    form: 'searchForm',
    enableReinitialize: true
})(withRouter(Search)))