import React from 'react';
import style from '~/assets/css/page.css';

export default class Label extends React.Component {
    render() {
        const { meta: { error, touched }, serverError = "", label } = this.props;
        return (
            <React.Fragment>
                {
                    touched === true && error && (
                        <p className={style.helpBlock}>{error}</p>
                    )
                }
                {
                    touched === true && !error && serverError && (
                        <p className={style.helpBlock}>{serverError}</p>
                    )
                }
            </React.Fragment>
        )
    }
}