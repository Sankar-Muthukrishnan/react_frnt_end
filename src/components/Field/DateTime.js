import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import Label from "./Label";
import Error from "./Error";
import style from '~/assets/css/page.css';

const PopperContainer = ({ children }) => (
    ReactDOM.createPortal(
        children,
        document.querySelector('body'),
    )
);

class DateTime extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.input.value ? moment(props.input.value * 1000).toDate() : moment().toDate()
        }
    }

    componentDidMount() {
        if (!this.props.input.value) {
            this.props.input.onChange(Math.floor(moment.unix(this.state.value).valueOf() / 1000000))
        }
    }

    componentDidUpdate(prevProps) {
        const { input: { value } } = this.props;
        if ((prevProps.input.value != value)) {
            this.setState({
                value: value ? moment(value * 1000).toDate() : moment().toDate()
            })
        }
    }

    render() {
        const { required = false, label, meta: { error, touched } } = this.props;
        const { value } = this.state;
        return (
            <div className={`${style.formGroup} ${touched && error ? " " + style.hasError : ""}`}>
                <Label text={label} required={required} focus={true} />
                <div className={style.textbox}>
                    <DatePicker
                        popperContainer={PopperContainer}
                        placeholderText="Pick a date & time"
                        selected={value}
                        onChange={this.onChangeHandler}
                        dateFormat="dd MMM yyyy"
                        showTimeSelect
                    />
                </div>
                <Error {...this.props} />
            </div>
        )
    }

    onChangeHandler = (value) => {
        this.props.input.onChange(Math.floor(moment.unix(value).valueOf() / 1000000))
    }
}
export default DateTime;