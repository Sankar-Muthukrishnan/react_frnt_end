import React from 'react';
import DatePicker from 'react-datepicker';
import Label from "./Label";
import Error from "./Error";
import style from '~/assets/css/page.css';

class Date extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            value: this.onChangeValue()
        }
    }

    componentDidMount() {
        if (!this.props.input.value) {
            this.props.input.onChange(this.onChangeValue())
        }
    }

    componentDidUpdate(prevProps) {
        const { input: { value } } = this.props;
        if ((prevProps.input.value != value)) {
            this.setState({
                value: this.onChangeValue()
            })
        }
    }

    render() {
        const { required = false, label, meta: { error, touched } } = this.props;
        const { value } = this.state;
        return (
            <div className={`${style.formGroup} ${touched && error ? " " + style.hasError : ""}`}>
                <Label text={label} required={required} focus={true} />
                <div className={style.textbox}>
                    <DatePicker
                        placeholderText="Pick a date"
                        selected={value}
                        onChange={this.onChangeHandler}
                        dateFormat="dd MMM yyyy"
                    />
                </div>
                <Error {...this.props} />
            </div>
        )
    }

    onChangeValue = () => {
        const { time = true, input: { value } } = this.props;
        return value != 'Invalid Date' &&  value != ''  ? moment(value, 'yyyy-MM-DD').toDate() : moment().toDate()
    }

    onChangeHandler = (value) => {
        this.props.input.onChange(moment(value).format("yyyy-MM-DD"))
    }
}
export default Date;