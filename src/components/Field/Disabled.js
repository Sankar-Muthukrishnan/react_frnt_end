import React from 'react';
import Label from "./Label";
import style from '~/assets/css/page.css';

class Disabled extends React.Component {
    render() {
        const {
            label,
            value,
            fetching,
            required
        } = this.props;
        return (
            <div className={`${style.formGroup} ${style.disabledField}`}>
                {
                    fetching && (
                        <div className={style.textbox}>
                            <div className={style.placeholder}>&nbsp;</div>
                        </div>
                    )
                }
                {
                    !fetching && <div className={style.textbox} dangerouslySetInnerHTML={{ __html: value }}></div>
                }
                <Label text={label} required={required} focus={true} />
            </div>
        );
    };
};

export default Disabled;