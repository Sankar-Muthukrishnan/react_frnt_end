import React from 'react';
import Label from "./Label";
import Error from "./Error";
import style from '~/assets/css/page.css';

class DurationInput extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            valueEntered: props.input.value ? true : false,
            inputValue: props.input.value ? this.timetostring(props.input.value) : ""
        }
    }

    componentDidUpdate(prevProps) {
        const { input: { value } } = this.props;
        if (value != prevProps.input.value) {
            this.setState({
                valueEntered: value ? true : false,
                inputValue: value ? this.timetostring(value) : ""
            });
        }
    }

    render() {
        const { label, required = false, type, meta: { error, touched } } = this.props;
        const { inputValue } = this.state;
        return (
            <div className={`${style.formGroup}${touched && error ? " " + style.hasError : ""}`}>
                <Label text={label} required={required} focus={true} />
                <input 
                    type={type} 
                    value={inputValue} 
                    placeholder="00:00" 
                    className={style.textbox} 
                    onKeyPress={this.onKeyPressHandler} 
                    onChange={this.onChangeHandler}
                />
                <Error {...this.props} />
            </div>
        );
    }

    onKeyPressHandler = (e) => {
        let value = e.target.value;
        let code = e.which || e.keyCode;
        let cursor = e.target.selectionStart;
        let colon = value.indexOf(":");
        if(code === 13) {
            //Enter press
        }
        else if ((code < 48 || code > 58) || (code === 58 && colon !== -1)) {
            e.preventDefault();
        }
        else if (colon !== -1) {
            if (code === 58) {
                //Do not allow double colon
                e.preventDefault();
            }
            else if (cursor <= colon) {
                //Allow anyting to be typed before colon
            }
            else {
                let time = this.stringtotime(value);
                if (colon + 1 == cursor && (code > 53)) {
                    //Don't allow first character after colon to be more than 5
                    e.preventDefault();
                }
                else if (time.minString.length >= 2) {
                    //Don't allow more than two characters after colon
                    e.preventDefault();
                }
            }
        }
    }

    onChangeHandler = (e) => {
        let value = e.target.value;
        this.setState({
            inputValue: value
        })
        if (value.trim() == "") {
            value = "";
        }
        else {
            let time = this.stringtotime(value);
            value = (time.hr * 60) + time.min;
        }
        this.props.input.onChange(value)
    }

    stringtotime = (value) => {
        let x = value.split(":");
        let hr = x[0].trim() == "" ? 0 : parseInt(x[0]);
        let mins = x[1] ? x[1].trim() : "";
        let min = this.calculateMins(mins);
        if (mins > 59) {
            hr += Math.floor(min / 60);
            min = min % 60;
        }
        return {
            hr: hr,
            min: min,
            hrString: x[0],
            minString: mins
        }
    }

    timetostring = (initial) => {
        let hr = Math.floor(initial / 60);
        let min = Math.floor(initial % 60);
        let value = hr < 10 ? "0" + hr : hr.toString();
        value += ":" + (min < 10 ? "0" + min : min.toString());
        return value;
    }

    calculateMins = (mins) => {
        if (mins.length == 0)
            return 0;
        else if (mins.length == 1)
            return parseInt(mins) * 10;
        return parseInt(mins);
    }
}
export default DurationInput;