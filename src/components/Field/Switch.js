import React from 'react';
import style from '~/assets/css/page.css';

class Switch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            valueEntered: props.input.value ? 1 : 0
        }
    }

    componentDidUpdate(prevProps) {
        const { input: { value } } = this.props;
        if (value != prevProps.input.value) {
            this.setState({
                valueEntered: value ? 1 : 0
            })
        }
    }

    render() {
        const { valueEntered } = this.state;
        return (
            <div className={style.switch}>
                <div className={style.switcher}>
                    <a className={`${style.slider} ${this.cssClass(valueEntered)}`} onClick={this.onClickHandler}></a>
                </div>
            </div>
        )
    }

    onClickHandler = (e) => {
        const { valueEntered } = this.state;
        const { input, onChange } = this.props;
        let newValue = valueEntered ? 0 : 1;
        input.onChange(newValue);
        if (onChange) {
            onChange(newValue);
        }
    }

    cssClass = (valueEntered) => {
        if (valueEntered == 1)
            return style.switchOn;
        return style.switchOff;
    }
}

export default Switch;