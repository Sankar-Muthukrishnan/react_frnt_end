import React from 'react';
import style from '~/assets/css/page.css';

class ServerError extends React.Component {
    render() {
        const { error } = this.props;

        return (
            <div>
                {error && (
                    <div className={`${field.hasError} ${field.staticError}`}>
                        <p className={field.helpBlock}>{error}</p>
                    </div>
                )}
            </div>
        );
    };
};

export default ServerError;