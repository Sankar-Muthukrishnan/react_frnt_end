import React from 'react';
import { Link } from 'react-router-dom';
import Spinner from '../Spinner';
import style from '~/assets/css/page.css';

export default class Button extends React.Component {
    static defaultProps = {
        disabled: false,
        link: "",
        target: '_self',
        submit: false,
        classes: "",
        color: "#02269E",
        fetching: false,
        label: "",
        onClick: null
    }

    getSizeClass = () => {
        if (this.props.size === "small")
            return style.buttonSmall;

        return style.buttonLarge;
    }

    render() {
        const { color, classes, submit, label, target, fetching, link, disabled, onClick } = this.props;
        if (fetching === true) {
            return (
                <div className={`${style.button}${classes ? " " + classes : ""}`}>
                    <div className={style.fetching}>
                        <Spinner color={color} />
                    </div>
                    <span>&nbsp;</span>
                </div>
            )
        }

        if (disabled === true) {
            return (
                <div className={`${style.button}${classes ? " " + classes : ""} ${style.buttonDisabled}`}>
                    <span dangerouslySetInnerHTML={{ __html: label }}></span>
                </div>
            )
        }

        if (submit === true) {
            return (
                <button type="submit" className={`${style.button}${classes ? " " + classes : ""}`}>
                    <span dangerouslySetInnerHTML={{ __html: label }}></span>
                </button>
            )
        }

        if (!link) {
            return (
                <a className={`${style.button}${classes ? " " + classes : ""}`} onClick={onClick}>
                    <span dangerouslySetInnerHTML={{ __html: label }}></span>
                </a>
            )
        }

        return (
            <Link to={link} className={`${style.button}${classes ? " " + classes : ""}`} target={target} onClick={onClick}>
                <span dangerouslySetInnerHTML={{ __html: label }}></span>
            </Link>
        )
    }
}