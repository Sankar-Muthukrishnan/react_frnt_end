import React from 'react';
import style from '~/assets/css/page.css';

export default class Label extends React.Component {
    static defaultProps = {
        focus: false
    };

    render() {
        const { text, required, focus } = this.props;
        if(text === false) {
            return null;
        }
        
        return (
            <div className={`${style.controlLabel}${focus === true ? " " + style.focus : ""}`}>
                <span>{text}</span>
                {
                    required && (
                        <span className={style.required}>*</span>
                    )
                }
            </div>
        )
    }
}