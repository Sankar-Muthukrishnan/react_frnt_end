import React from 'react';
import Label from "./Label";
import Error from "./Error";
import style from '~/assets/css/page.css';

export default class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            focus: false,
            valueEntered: props.input.value.toString() ? true : false,
            inputValue: props.input.value,
            defaultValue: props.input.value,
        };
    }

    static defaultProps = {
        required: false,
        type: "text",
        disabled: false,
        onKeyPress: () => { }
    }

    componentDidUpdate(prevProps) {
        const { input: { value } } = this.props;
        if (value != prevProps.input.value) {
            this.setState({
                valueEntered: value.toString() ? true : false,
                inputValue: value
            })
        }
    }

    onFocusHandler = () => {
        this.setState({
            focus: true
        });
    }

    onBlurHandler = () => {
        this.setState({
            focus: false
        });
    }

    handleEditorChange = (content) => {
        const { input: { onChange } } = this.props;
        onChange(content);
    }

    getInput = () => {
        const { focus, defaultValue } = this.state;
        const { type, input, onKeyPress, meta: { error, touched }, ...other } = this.props;
        if (type === "textarea") {
            return (
                <textarea {...input} className={style.textbox} onFocus={this.onFocusHandler} onBlur={(e) => { input.onBlur(e); setTimeout(() => this.onBlurHandler()) }}></textarea>
            )
        }
        return (
            <input type={type} {...input} {...other} readOnly={!focus} className={style.textbox} onFocus={this.onFocusHandler} onBlur={(e) => { input.onBlur(e); setTimeout(() => this.onBlurHandler()) }} />
        )
    }

    render() {
        const { label, required, meta: { error, touched }, type, classes } = this.props;
        const { focus, valueEntered } = this.state;
        return (
            <div className={`${style.formGroup}${touched && error ? " " + style.hasError : ""} ${classes?classes:''}`}>
                {
                    label !== "false" && type !== "editor" && (
                        <Label text={label} required={required} focus={focus || valueEntered ? true : false} />
                    )
                }
                {this.getInput()}
                <Error {...this.props} />
            </div>
        )
    }
}