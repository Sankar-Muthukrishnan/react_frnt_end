import React from 'react';
import { connect } from 'react-redux';
import style from '~/assets/css/page.css';
import { Link } from 'react-router-dom';
import { removeClass, addClass } from '~/components/Utils';
import { reduxForm } from 'redux-form';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: null,
        };
    }

    componentDidMount() {
        window.addEventListener("load", this.resize, false);
        window.addEventListener("resize", this.resize, false);
        setTimeout(() => {
            this.setState({
                profile: JSON.parse(window.localStorage.getItem('profile'))
            });
        }, 1000);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.uploads !== this.props.uploads || prevProps.user != this.props.user) {
            setTimeout(() => {
                this.setState({
                    profile: JSON.parse(window.localStorage.getItem('profile'))
                });
            }, 1000);
        }
    }

    resize = () => {
        if (window.innerWidth <= 1100 && window.innerWidth > 768) {
            addClass('tag', `html`, `${style.showLeftSide}`);
            removeClass('class', `${style.navLink} ${style.arrow}`, `${style.showSubMenu}`);
            removeClass('class', `${style.arrow}`, `${style.ShowSubMenu}`);
        }
        else if (window.innerWidth <= 768) {
            removeClass('tag', `html`, `${style.showLeftSide}`);
        }
    }

    showleftside = () => {
        if (window.innerWidth > 1100) {
            $('html').toggleClass(`${style.showLeftSide}`);
            removeClass('class', `${style.navLink} ${style.arrow}`, `${style.showSubMenu}`);
            $(`.${style.subNav}`).removeAttr("style");
        }

        if (window.innerWidth <= 768) {
            $('html').toggleClass(`${style.openLeftSide}`);
        }
    }

    render() {
        const { profile } = this.state;
        const { data = false } = this.props;
        return (
            <div className={`${style.topHeader}`}>
                <div className={`${style.headerWrap}`}>
                    <a className={`${style.headLink} ${style.col}`} href='#' onClick={() => this.showleftside()}><span><i className={`far fa-bars ${style.icn}`}></i></span></a>
                    <div className={`${style.options}`}>
                        <div className={style.vertiCenter}>
                            {
                                data !== false && data
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    };
};

const mapStateToProps = (state) => {
    return {
        ...state.header
    };
};

export default connect(mapStateToProps)(reduxForm({
    form: 'search',
    enableReinitialize: true
})(Header));