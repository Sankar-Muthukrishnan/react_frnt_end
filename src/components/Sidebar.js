import React from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';
import style from '~/assets/css/page.css';
import { Link, NavLink } from 'react-router-dom';
import logo from '~/assets/images/kgisl-gss-logo.svg';
import IconButton from './iconButton';
import Background from '~/components/Background';
import { removeClass, addClass } from './Utils';
import avatar from '~/assets/images/avatar.png';
import Search from '~/components/Search';

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: null
        };
    }

    componentDidMount() {
        document.getElementsByClassName(`${style.content}`)[0].addEventListener("click", this.onOutClick, false);
        setTimeout(() => {
            this.setState({
                profile: JSON.parse(window.localStorage.getItem('profile'))
            });
        }, 1000);
    }

    onclose = () => {
        addClass('class', `${style.searchPop}`, `${style.closeSearchPop}`);
        setTimeout(() => {
            this.setState({
                popup: false
            })
        }, 800)
    }

    showsub = (se) => {
        if ($('html').hasClass(`${style.showLeftSide}`)) {
            $(`.${style.subNav}`).removeAttr('style');
            if ($('#' + se).hasClass(`${style.showSubMenu}`)) {
                removeClass('id', se, `${style.showSubMenu}`)
            }
            else {
                $('#' + se).closest(`.${style.navLinkWrap}`).find(`.${style.showSubMenu}`).removeClass(`${style.showSubMenu}`);
                addClass('id', se, `${style.showSubMenu}`);
            }
        }
        else {
            if ($('#' + se).hasClass(`${style.showSubMenu}`)) {
                removeClass('id', se, `${style.showSubMenu}`)
                $('#' + se).find(`.${style.subNav}`).slideUp(150);
            }
            else {
                $('#' + se).closest(`.${style.navLinkWrap}`).find(`.${style.showSubMenu}`).find(`.${style.subNav}`).slideUp(150);
                $('#' + se).closest(`.${style.navLinkWrap}`).find(`.${style.showSubMenu}`).removeClass(`${style.showSubMenu}`);
                addClass('id', se, `${style.showSubMenu}`);
                $('#' + se).find(`.${style.subNav}`).slideDown(150);
            }
        }
    }

    onOutClick = () => {
        if ($('html').hasClass(`${style.showLeftSide}`)) {
            removeClass('class', `${style.arrow}`, `${style.showSubMenu}`);
        }
        else if ($(`.${style.subLink}`).hasClass(`${style.active}`)) {
        }
        else {
            removeClass('class', `${style.arrow}`, `${style.showSubMenu}`);
            $(`.${style.subNav}`).slideUp(150);
        }

    }

    componentDidUpdate(prevProps) {
        if (prevProps.uploads !== this.props.uploads || prevProps.user != this.props.user) {
            setTimeout(() => {
                this.setState({
                    profile: JSON.parse(window.localStorage.getItem('profile'))
                });
            }, 1000);
        }
    }

    onLogout = () => {
        const { history } = this.props;
        window.localStorage.removeItem('access_token');
        window.location = `${PREFIX_URL}/`;
    }

    render() {
        const { profile, popup = false } = this.state;
        const navs = [
            {
                title: "Dashboard",
                url: "dashboard",
                classes: `${style.icon} fas fa-tachometer-alt`,
                submenu: true,
                sling: [
                    {
                        title: "Dashboard v1",
                        url: "dashboard"
                    },
                    {
                        title: "Dashboard v2",
                        url: "dashboard"
                    }
                ]
            },
            {
                title: "Users",
                url: "users",
                classes: `${style.icon} fas fa-users`,
                submenu: true,
                sling: [
                    {
                        title: "Add user",
                        url: "users/create"
                    }
                ]
            }
        ];
        return (
            <div>
                <div className={`${style.sideBarWrap}`}>
                    <div className={`${style.wrapper}`}>
                        <div className={`${style.logoArea} ${style.row}`}>
                            <div className={`${style.col} ${style.left}`}>
                                <Link to={`${PREFIX_URL}` + `/home`}><img src={logo} alt="" className={style.logoImg} /></Link>
                            </div>
                            <div className={`${style.col} ${style.links} ${style.right}`}>
                                <IconButton classes={`${style.homeIcon} ${style.button}`} icon={`home`} link={PREFIX_URL + "/home"} />
                                <IconButton classes={`${style.button}`} icon={true} icon={`email`} link={PREFIX_URL + "/home"} />
                            </div>
                        </div>
                        <div className={`${style.scrollable}`}>
                            <div className={`${style.userAr} `}>
                                <div className={`${style.row}`}>
                                    <div className={`${style.col} ${style.left}`}>
                                        <div className={style.avtarWrapper}>
                                            <div className={style.avtarImageWrapper}>
                                                {
                                                    get(profile, 'image') ?
                                                        <Background prefix={get(profile, 'image.prefix')} type={get(profile, 'image.type')} name={get(profile, 'image.name')} extension={get(profile, 'image.extension')} size={'100x100'} />
                                                        : <div className={`${style.bsz}`}>
                                                            <div className={`${style.bgimage}`} style={{ backgroundImage: `url(${avatar})` }}></div>
                                                            <img src={avatar} />
                                                        </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className={`${style.col} ${style.right}`}>
                                        <p className={`${style.userName}`}><span>{get(profile, 'first_name')} {get(profile, 'surname')}</span></p>
                                        <p className={`${style.userStatus}`}><span>{get(profile, 'quote')}</span></p>
                                    </div>
                                </div>
                            </div>
                            <div className={`${style.navLinkWrap}`}>
                                {
                                    navs.map((nav, i) => {
                                        return (
                                            <div className={`${style.navLink} ${nav.submenu ? style.arrow : ''}`} key={i} id={i}>
                                                <NavLink className={`${style.link}`} to={nav.popup ? { javascript: void (0) } : PREFIX_URL + "/" + nav.url} onClick={nav.popup ? () => this.setState({ popup: true }) : () => this.showsub(i)} ><i className={nav.classes}></i><span className={`${style.col} ${style.navLabel}`}>{nav.title}</span></NavLink>
                                                {nav.submenu && nav.sling &&
                                                    <div className={`${style.subNav}`}>
                                                        {nav.sling && nav.sling.map((snav, j) => {
                                                            return (
                                                                <NavLink key={j} className={`${style.link} ${style.subLink}`} to={PREFIX_URL + "/" + snav.url}><span>{snav.title}</span></NavLink>
                                                            )
                                                        })
                                                        }
                                                    </div>
                                                }
                                            </div>
                                        )
                                    })

                                }
                            </div>
                        </div>
                        <div className={style.logoutArea}>
                            <a className={`${style.logoutBtn}`} onClick={() => this.onLogout()}>
                                <span>Log out</span>
                                <i className="material-icons">power_settings_new</i>
                            </a>
                        </div>
                    </div>
                </div>
                <div className={`${style.searchPop} ${this.state.popup ? `${style.showSearchPop}` : ''}`}>
                    {popup !== false &&
                        <Search onclose={() => this.onclose()} />
                    }
                </div>
            </div>
        );
    };
};

const mapStateToProps = (state, props) => {
    return {
        ...state.list
    };
};

export default connect(mapStateToProps)(Sidebar);

