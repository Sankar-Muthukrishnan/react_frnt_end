import React from 'react';
import { Route } from 'react-router-dom'
import Auth from './index';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        return (
            <Auth Component={Component} p={props} />
        )
    }} />
)