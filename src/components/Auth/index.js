import React from 'react';
import { connect } from "react-redux";
import Sidebar from '../../components/Sidebar';
import Header from '../../components/Header';
import { getUser } from '../../actions';
import { getAccessToken } from "../../components/Utils";
import style from '~/assets/css/page.css';

class Auth extends React.Component {
    componentDidMount() {
        if (!getAccessToken()) {
            this.props.p.history.push("/login");
            return false;
        }
        else {
            this.props.getUser();
        }
    }

    componentDidUpdate(prevProps) {
        const { Component } = this.props;
        if (prevProps.Component !== Component) {
            $(window).scrollTop(0);
        }
    }

    render() {
        const { Component, p, user } = this.props;
        if (!user) {
            return null;
        }
        return (
            <>
                <Sidebar />
                <div className={style.content}>
                    <Header />
                    <Component {...p} />
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.credentials
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getUser: () => dispatch(getUser())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth)