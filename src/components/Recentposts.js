import React from 'react';
import style from '~/assets/css/page.css';
import Background from '~/components/Background';
import { get } from 'lodash';
import IconButton from './iconButton';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { connect } from 'react-redux';

class Recentposts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            droppableDirection: "horizontal"
        };
    }

    componentDidMount() {
        window.addEventListener("resize", this.resize, false);
    }

    resize = () => {
        if ($(window).width() <= 1000) {
            this.setState({
                droppableDirection: "vertical"
            })
        } else {
            this.setState({
                droppableDirection: "horizontal"
            })
        }
    }

    render() {
        const { profile, recentposts } = this.props;

        return (
            <Droppable droppableId="droppable-2" type="recentPost" direction={this.state.droppableDirection}>
                {(provided, snapshot) => (
                    <div className={`${style.recentPostAr} ${style.outer}`} ref={provided.innerRef} style={{ backgroundColor: snapshot.isDraggingOver ? 'aliceblue' : 'transparent' }} {...provided.droppableProps}>
                        {recentposts.map((po, n) => {
                            let dropId = 'postId' + n;
                            return (
                                <Draggable draggableId={dropId} index={n} key={n}>
                                    {(provided, snapshot) => (
                                        <div className={`${style.recentPostWrap}`} id={'post' + po.id} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                            <div className={style.recentPost}>
                                                <div className={`${style.postImageAr} ${style.relative}`}>
                                                    <div className={`${style.recentActionAr}`}>
                                                        <IconButton icon={`push_pin`} classes={`${style.tileIcon} ${style.pinItem}`} clickdo={() => this.props.pinnable('post' + po.id)} />
                                                        <IconButton icon={`refresh`} classes={`${style.tileIcon}`} />
                                                        <IconButton icon={`clear`} classes={`${style.tileIcon} ${style.delItem}`} />
                                                    </div>
                                                    {profile &&
                                                        <div className={`${style.relative} ${style.recentImg}`}>
                                                            <div className={style.sizer}></div>
                                                            <Background prefix={get(profile, 'image.prefix')} type={get(profile, 'image.type')} name={get(profile, 'image.name')} extension={get(profile, 'image.extension')} size={'340x210'} />
                                                        </div>
                                                    }
                                                </div>
                                                <div className={style.commonWrapper}>
                                                    <div className={style.recentContent}>
                                                        <div className={style.recentPostTitle}>
                                                            {po.title}
                                                        </div>
                                                        <div className={style.recentShortCode}>
                                                            {po.content}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </Draggable>
                            );
                        })}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        )
    };
};

export default connect()(Recentposts);