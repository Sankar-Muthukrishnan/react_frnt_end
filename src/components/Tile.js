import React from 'react';
import style from '~/assets/css/page.css';
import { Button } from '~/components/Field';
import {  Droppable, Draggable } from 'react-beautiful-dnd';
import IconButton from './iconButton';
import { connect } from 'react-redux';

class Tile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            droppableDirection: "horizontal"
        };
    }

    componentDidMount() {
        window.addEventListener("resize",this.resize, false);
    }

    resize = () => {
        if(window.innerWidth <= 1000) {
            this.setState({
                droppableDirection: "vertical"
            })
        }
        else {
            this.setState({
                droppableDirection: "horizontal"
            })
        }
    }

    render() {
        const { tiles } = this.props;
        return (
            <Droppable droppableId="droppable-1" type="tile" direction= {this.state.droppableDirection}>
                {(provided, snapshot) => (
                    <div className={style.outer}
                        ref={provided.innerRef}
                        style={{ backgroundColor: snapshot.isDraggingOver ? 'aliceblue' : 'transparent' }}
                        {...provided.droppableProps}
                    >
                        {
                            tiles.map((det, n) => {
                                let id = "tile"+n
                                return (
                                    <Draggable draggableId={id} index={n} key={n}>
                                        {(provided, snapshot) => (
                                            <div className={style.tileAr} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                                <div className={`${style.tileWrap} ${style.commonWrapper}`}>
                                                    <div className={style.relative}>
                                                        <div className={`${style.col} ${style.tileTitleAr}`}>
                                                            <span className={style.tileTitle}>{det.title}</span>
                                                        </div>
                                                        <div className={`${style.col} ${style.tileActionAr}`}>
                                                            <IconButton icon={true} icon={`refresh`} classes={`${style.tileIcon}`} />
                                                            <IconButton icon={true} icon={`clear`} classes={`${style.tileIcon}`} />
                                                        </div>
                                                    </div>
                                                    <div className={style.tileMiddle}>
                                                        <div className={style.col}>
                                                            <div className={style.countType}><span>Overall Visits</span></div>
                                                            <div className={style.countNumber}><span>{det.overall}</span></div>
                                                        </div>
                                                        <div className={style.col}>
                                                            <div className={style.countType}><span>Weekly</span></div>
                                                            <div className={style.countNumber}><span>{det.today}</span></div>
                                                        </div>
                                                        <div className={style.col}>
                                                            <div className={style.countType}><span>Monthly</span></div>
                                                            <div className={style.countNumber}><span>{det.monthly}</span></div>
                                                        </div>
                                                    </div>
                                                    <div className={style.tileProgress}>
                                                        <div className={style.progressBar}>
                                                            <span className={style.statusProgress}></span>
                                                        </div>
                                                        <div className={style.progressText}>
                                                            <span className={style.highlighted}>{det.percentage} higher</span>
                                                            <span className={style.Normal}> than last month</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )}
                                    </Draggable>
                                )
                            })}
                            {provided.placeholder}
                    </div>
                )}
            </Droppable>
        );
    };
}


export default connect()(Tile);