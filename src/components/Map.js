import React, { useState } from 'react';
import style from '~/assets/css/page.css';
import markerIcon from '~/assets/images/marker_icon.png';
import { GoogleMap, withScriptjs, withGoogleMap, Marker, InfoWindow } from "react-google-maps";

const park = [
    {
        parkId: 1,
        park: 'Scotland',
        description: 'description',
        lat: 57.71588513,
        lng: -5.41625977
    },
    {
        parkId: 2,
        park: 'London',
        description: 'description',
        lat: 51.23440735,
        lng: -0.04394531
    },
    {
        parkId: 3,
        park: 'Paris',
        description: 'description',
        lat: 48.16608542,
        lng: 2.76855469
    },
    {
        parkId: 4,
        park: 'Netherland',
        description: 'description',
        lat: 51.67255515,
        lng: 7.25097656
    }
];

function map() {
    const [selectedPark, setSelectedPark] = useState(null);
    return (
        <GoogleMap
            defaultZoom={4}
            defaultCenter={{ lat: 51.514947, lng: -0.0930461 }}>
            {park.map((pa, i) => (
                <Marker key={i}
                    icon={{
                        url: markerIcon,
                        scaledSize: new window.google.maps.Size(30, 30),
                    }}
                    animation={google.maps.Animation.DROP}
                    position={{
                        lat: pa.lat,
                        lng: pa.lng
                    }}
                    onClick={() => {
                        setSelectedPark(pa);
                    }}
                    title={pa.park}
                />
            ))}

            {selectedPark && (
                <InfoWindow
                    position={{
                        lat: selectedPark.lat,
                        lng: selectedPark.lng
                    }}
                    onCloseClick={() => {
                        setSelectedPark(null);
                    }}
                >
                    <div>
                        <h3 className={style.mapTitle}>{selectedPark.park}</h3>
                        <p className={style.mapDescrip}>{selectedPark.description}</p>
                    </div>

                </InfoWindow>
            )}
        </GoogleMap>
    );
}

const WrappedMap = withScriptjs(withGoogleMap(map));
class Map extends React.Component {
    render() {
        return (
            <WrappedMap
                googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyADWUF2dNq1jfTi8m5Wn8swPUdSq58iM3U`}
                loadingElement={<div style={{ height: "100%" }} />}
                containerElement={<div style={{ height: "100%" }} />}
                mapElement={<div style={{ height: "100%" }} />} />
        )
    };
};

export default Map;