import React from 'react';
import { Link } from 'react-router-dom';
import style from '~/assets/css/page.css';

class Breadcrumbs extends React.Component {
    render() {
        const { current, previousPath, previous } = this.props;
        return (
            <div className={`${style.breadWrap}`}>
                {previousPath && previous &&
                    <div className={`${style.col} ${style.homeBread}`}>
                        <Link to={previousPath}>{previous}</Link>
                    </div>
                }
                {current &&
                    <div className={`${style.col} ${style.currentBread}`}>
                        <span className={`${style.currentPage}`}>{current}</span>
                    </div>
                }
            </div>
        );
    };
};
export default Breadcrumbs;