import React from 'react';
import Spinner from './Spinner';
import { get } from 'lodash';
import style from '~/assets/css/page.css';
import Background from '~/components/Background';

export default class SingleFile extends React.Component {
    render() {
        const { remove, valid, success, data, complete, progress, onRemoveHandler } = this.props;
        if (remove || !valid) {
            return null;
        }
        return (
             <div className={style.uploadProgress}>
                {
                    complete === false && (
                        <Spinner color="#000" />
                    )
                }
                {
                    success === true && data && (
                        <Background prefix={get(data,'prefix')} type={get(data,'type')} orgname={get(data,'orgname')} name={get(data,'name')} extension={get(data,'extension')} size="100x100" />
                    )
                }
            </div>
        )
    }
}
      